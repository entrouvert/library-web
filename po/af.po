# Afrikaans translation of library-web (library.gnome.org).
# This file is distributed under the same license as the library-web package.
# F Wolff <friedel@translate.org.za>, 2008
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-01-10 15:47+0100\n"
"PO-Revision-Date: 2008-01-07 13:50+0200\n"
"Last-Translator: F Wolff <friedel@translate.org.za>\n"
"Language-Team: Afrikaans localisation list <translate-discuss-af@lists."
"sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/catalog.xml.in.h:1
msgid "API References"
msgstr "API-verwysings"

#: ../data/catalog.xml.in.h:2
msgid "About"
msgstr "Aangaande"

#: ../data/catalog.xml.in.h:3
msgid ""
"Across the world, there are many large to small deployments of GNOME, with "
"their specific needs, and system administrators to manage them. Here you "
"will find information on tools and methods to work with many GNOME desktops."
msgstr ""
"Regoor die wêreld word GNOME ontplooi, in opsette klein en groot, elk met "
"spesifieke behoeftes en stelseladministrateurs om dit te bestuur. Hier vind "
"mens inligting aangaande gereedskap en metodes om met klomp GNOME-rekenaars "
"te werk."

#: ../data/catalog.xml.in.h:4
msgid "Administrators"
msgstr "Administrateurs"

#: ../data/catalog.xml.in.h:5
msgid "Art"
msgstr "Kuns"

#: ../data/catalog.xml.in.h:6
msgid "Available Languages:"
msgstr "Beskikbare tale"

#: ../data/catalog.xml.in.h:7
msgid "Available Versions:"
msgstr "Beskikbare weergawes:"

#: ../data/catalog.xml.in.h:8
msgid "Community"
msgstr "Gemeenskap"

#: ../data/catalog.xml.in.h:9
msgid "Developers"
msgstr "Ontwikkelaars"

#: ../data/catalog.xml.in.h:10
msgid "Development"
msgstr "Ontwikkeling"

#: ../data/catalog.xml.in.h:11
msgid "Downloads"
msgstr "Aflaai"

#: ../data/catalog.xml.in.h:12
msgid ""
"Even though it's extremely user-friendly, GNOME is a large and complex "
"system, and thus, requires some learning to use to the fullest. To make that "
"easier, we've provided some very useful documentation."
msgstr ""
"Alhoewel GNOME baie gebruikersvriendelik is, is dit 'n groot en komplekse "
"stelsel, en benodig dus bietjie leerwerk om die meeste daaruit te put. Om "
"dit makliker te maak, verskaf ons nuttige dokumentasie."

#: ../data/catalog.xml.in.h:13
msgid ""
"For those who develop, or are interested in developing GNOME and "
"applications for GNOME. You will find developer documentation and "
"information on how to get involved, and much more."
msgstr ""
"Vir die wat ontwikkel of belangstel in die ontwikkeling van GNOME of "
"programme vir GNOME. Hier is ontwikkelingsdokumentasie en inligting oor hoe "
"om betrokke te raak en nogwat."

#: ../data/catalog.xml.in.h:14
msgid "Frequently Asked Questions"
msgstr "Algemene vrae"

#: ../data/catalog.xml.in.h:15
msgid "GNOME Documentation Library"
msgstr "GNOME-dokumentasiebiblioteek"

#: ../data/catalog.xml.in.h:16
msgid "GNOME Documentation Project"
msgstr "GNOME-dokumentasieprojek"

#: ../data/catalog.xml.in.h:17
msgid "Guidelines"
msgstr "Riglyne"

#: ../data/catalog.xml.in.h:18 ../data/overlay.xml.in.h:28
msgid "Guides"
msgstr "Gidse"

#: ../data/catalog.xml.in.h:19
msgid "Home"
msgstr "Tuis"

#: ../data/catalog.xml.in.h:20
msgid "Lookup Symbol"
msgstr "Soek na simbool"

#: ../data/catalog.xml.in.h:21
msgid "Manuals"
msgstr "Handleidings"

#: ../data/catalog.xml.in.h:22
msgid "News"
msgstr "Nuus"

#: ../data/catalog.xml.in.h:23
msgid ""
"Note the API references are usually available as packages in the "
"distributions and visible via the Devhelp tool."
msgstr ""
"Let op dat die API-verwysings gewoonlik beskikbaar is as pakkette in die "
"verspreidings en bekombaar is via die Devhelp-program."

#: ../data/catalog.xml.in.h:24
msgid "Others"
msgstr "Ander"

#: ../data/catalog.xml.in.h:25
msgid "Overview"
msgstr "Oorsig"

#: ../data/catalog.xml.in.h:26
msgid "Preferred language is loaded from a cookie."
msgstr "Voorkeurtaal word van 'n koekie gelaai."

#: ../data/catalog.xml.in.h:27
msgid "Projects"
msgstr "Projekte"

#: ../data/catalog.xml.in.h:28
msgid "Remove cookie"
msgstr "Skrap koekie"

#: ../data/catalog.xml.in.h:29
msgid "Search"
msgstr "Soek"

#: ../data/catalog.xml.in.h:30
msgid "See also:"
msgstr "Sien ook:"

#: ../data/catalog.xml.in.h:31
msgid "Standards"
msgstr "Standaarde"

#: ../data/catalog.xml.in.h:32
msgid "Support"
msgstr "Ondersteuning"

#: ../data/catalog.xml.in.h:33
msgid ""
"There is no translation of this documentation for your language; the "
"documentation in its original language is displayed instead."
msgstr ""
"Daar is geen vertaling van hierdie dokumentasie in u taal nie; die "
"dokumentasie word in die oorspronklike taal vertoon."

#: ../data/catalog.xml.in.h:34
msgid ""
"This module is heading towards planned deprecation. It will continue to be "
"supported and API/ABI stable throughout the GNOME 2.x series, but we do not "
"recommend using it in new applications unless you require functionality that "
"has not already been moved elsewhere."
msgstr ""

#: ../data/catalog.xml.in.h:35
msgid "Tools"
msgstr "Gereedskap"

#: ../data/catalog.xml.in.h:36
msgid "Tutorials"
msgstr "Tutoriale"

#: ../data/catalog.xml.in.h:37
msgid ""
"Unable to display document in preferred language loaded from cookie, as "
"translation probably does not exist."
msgstr ""
"Kan nie die dokument in die voorkeurtaal vertoon soos verkry van die koekie "
"nie. Die vertaling bestaan waarskynlik nie."

#: ../data/catalog.xml.in.h:38
msgid "Users"
msgstr "Gebruikers"

#: ../data/catalog.xml.in.h:39
msgid "White Papers"
msgstr "Witskrifte"

#: ../data/catalog.xml.in.h:40
msgid "development version"
msgstr "ontwikkelingsweergawe"

#: ../data/catalog.xml.in.h:41
msgid "documentation on development version"
msgstr "dokumentasie aangaande die ontwikkelingsweergawe"

#: ../data/catalog.xml.in.h:42
msgid "external resource"
msgstr "eksterne hulpbron"

#: ../data/catalog.xml.in.h:43
msgid "more versions, languages, or options..."
msgstr "meer weergawe, tale of keuses..."

#: ../data/catalog.xml.in.h:44
msgid "see other translations for this documentation"
msgstr "sien ander vertalings van hierdie dokumentasie"

#: ../data/overlay.xml.in.h:1
msgid "A library used for programming panel applets for the GNOME panel."
msgstr ""
"'n Bibltioteek vir programmering van miniprogramme vir die GNOME-paneel."

#: ../data/overlay.xml.in.h:2
msgid ""
"ATK provides the set of accessibility interfaces that are implemented by "
"other toolkits and applications. Using the ATK interfaces, accessibility "
"tools have full access to view and control running applications."
msgstr ""
"ATK verskaf 'n stel toeganklikheidskoppelvlakke wat deur ander "
"gereedskapstelle en programme geïmplementeer word. Deur gebruik te maak van "
"ATK-koppelvlakke het toeganklikheidsprogramme volle toegang om programme wat "
"uitvoer te bekyk en te beheer."

#: ../data/overlay.xml.in.h:3
msgid "Accessibility for Developers"
msgstr "Toeganklikheid vir ontwikkelaars"

#: ../data/overlay.xml.in.h:4
msgid ""
"An intermediate layer which isolates GTK+ from the details of the windowing "
"system."
msgstr ""
"'n Tussenin-laag wat GTK+ isoleer van die detail van die "
"vensterbestuurstelsel."

#: ../data/overlay.xml.in.h:5
msgid "An introduction to the new module interface of GNOME Deskbar-Applet."
msgstr ""
"'n Inleiding tot die nuwe modulekoppelvlak van die GNOME Deskbar-miniprogram."

#: ../data/overlay.xml.in.h:6
msgid "Bindings to other languages"
msgstr ""

#: ../data/overlay.xml.in.h:7
msgid ""
"Bonobo is a framework for creating reusable components for use in GNOME "
"applications, built on top of CORBA."
msgstr ""
"Bonobo is 'n raamwerk vir die skep van herbruikbare komponente in GNOME-"
"toepassings, en is gebou bo-op CORBA."

#: ../data/overlay.xml.in.h:8
msgid ""
"Bonobo-activation allows you to browse the available CORBA servers on your "
"system (running or not). It keeps track of the running servers so that if "
"you ask for a server which is already running, you will not start it again "
"but will reuse the already running one."
msgstr ""
"Bonobo-activation stel mens in staat om alle beskikbare CORBA-bedieners op "
"mens se stelsel te ondersoek (of hulle loop of nie). Dit hou tred van "
"bedieners wat loop sodat 'n versoek vir 'n bediener wat reeds loop, dié "
"bediener sal hergebruik in plaas van om dit weer te laat begin."

#: ../data/overlay.xml.in.h:9
msgid "C++ Interfaces for GTK+ and GNOME"
msgstr "C++-koppelvlakke vir GTK+ en GNOME"

#: ../data/overlay.xml.in.h:10
msgid "Desktop Administrators' Guide to GNOME Lockdown and Preconfiguration"
msgstr ""
"Werkskerm-administrateur se gids tot GNOME Lockdown en voorafopstelling"

#: ../data/overlay.xml.in.h:11
msgid "Desktop Application Autostart Specification"
msgstr "Outobegin-spesifikasie vir werkskermtoepassings"

#: ../data/overlay.xml.in.h:12
msgid "Desktop Entry Specification"
msgstr "Spesifikasie vir werkskerminskrywings"

#: ../data/overlay.xml.in.h:13
msgid "Development Tools"
msgstr "Ontwikkelingsgereedskap"

#: ../data/overlay.xml.in.h:14
msgid "Extended Window Manager Hints"
msgstr "Uitgebreide vensterbestuurder-wenke"

#: ../data/overlay.xml.in.h:15
msgid ""
"GAIL provides an implementation of the ATK interfaces for GTK+ and GNOME "
"libraries, allowing accessibility tools to interact with applications "
"written using these libraries."
msgstr ""
"GAIL verskaf 'n implementasie van die ATK-koppelvlakke vir GKT+- en GNOME-"
"biblioteke wat wisselwerking moontlik maak tussen toeganklikheidsprogramme "
"en programme wat hierdie biblioteke gebruik."

#: ../data/overlay.xml.in.h:16
msgid ""
"GConf provides the daemon and libraries for storing and retrieving "
"configuration data."
msgstr ""
"GConf verskaf die agtergrondproses en biblioteke vir die stoor en herwinning "
"van konfigurasiedata."

#: ../data/overlay.xml.in.h:17
msgid ""
"GLib provides the core application building blocks for libraries and "
"applications written in C. It provides the core object system used in GNOME, "
"the main loop implementation, and a large set of utility functions for "
"strings and common data structures."
msgstr ""
"GLib verskaf die kernboustene vir die skryf van biblioteke en toepassings in "
"C. Dit verskaf die kern-objekstelsel wat GNOME gebruik, die hoofprogramlus-"
"implementasie, en 'n groot versameling nutsfunksies vir stringe en algemene "
"datastrukture."

#: ../data/overlay.xml.in.h:18
msgid ""
"GMime is a powerful MIME (Multipurpose Internet Mail Extension) utility "
"library. It is meant for creating, editing, and parsing MIME messages and "
"structures."
msgstr ""
"GMime is 'n kragtige MIME-nutsbiblioteek (Multipurpose Internet Mail "
"Extension). Dit word gebruik vir die skep, redigering en analisering van "
"MIME-boodskappe en -strukture."

#: ../data/overlay.xml.in.h:19
msgid "GNOME Desktop Libraries"
msgstr "GNOME-werkskermbiblioteke"

#: ../data/overlay.xml.in.h:20
msgid "GNOME Developer Platform Libraries"
msgstr ""

#: ../data/overlay.xml.in.h:21
msgid "GObject provides the object system used for Pango and GTK+."
msgstr "GObject verskaf die objekstelsel wat gebruik word vir Pango en GTK+."

#: ../data/overlay.xml.in.h:22
msgid "GStreamer Reference Manual"
msgstr "GStreamer se verwysingshandleiding"

#: ../data/overlay.xml.in.h:23
msgid "GTK+ 2.0 Tutorial"
msgstr "GTK+ 2.0-tutoriaal"

#: ../data/overlay.xml.in.h:24
msgid "GTK+ FAQ"
msgstr "Algemene vrae oor GTK+"

#: ../data/overlay.xml.in.h:25
msgid ""
"GTK+ is the primary library used to construct user interfaces in GNOME "
"applications. It provides user interface controls and signal callbacks to "
"control user interfaces."
msgstr ""
"GTK+ is die hoofbiblioteek vir die bou van gebruikerkoppelvlakke in GNOME-"
"programme. Dit verskaf gebruikerkoppelvlak-kontroles en seinterugroep vir "
"die beheer van gebruikerkoppelvlakke."

#: ../data/overlay.xml.in.h:26
msgid ""
"GdkPixbuf is a library for image loading and manipulation. The GdkPixbuf "
"documentation contains both the programmer's guide and the API reference."
msgstr ""
"GdkPixbuf is 'n biblioteek vir die laai en manipulasie van beelde. Die "
"GdkPixbuf-dokumentasie bevat die programmeringsgids en API-verwysing."

#: ../data/overlay.xml.in.h:27
msgid ""
"GnomeVFS is the core library used to access files and folders in GNOME "
"applications. It provides a file system abstraction which allows "
"applications to access local and remote files with a single consistent API."
msgstr ""
"GnomeVFS is die hoofbiblioteek vir toegang tot lêers en gidse in GNOME-"
"programme. Dit verskaf 'n abstraksie vir die lêerstelsel wat programme in "
"staat stel om plaaslike en afgeleë lêers met 'n enkele konsekwente API te "
"vekry."

#: ../data/overlay.xml.in.h:29
msgid "Icon Naming Specification"
msgstr "Naamspesifikasie vir ikone"

#: ../data/overlay.xml.in.h:30
msgid "Icon Theme Specification"
msgstr "Spesifikasie vir ikoontemas"

#: ../data/overlay.xml.in.h:31
msgid ""
"Libart functions - Libart handles the drawing capabilities in GNOME. All "
"complex rendering is handled here."
msgstr ""
"Libart-funksies - Libart hanteer die tekenvermons in GNOME. Alle komplekse "
"tekeninge word hier hanteer."

#: ../data/overlay.xml.in.h:32
msgid ""
"Libglade is a library for constructing user interfaces dynamically from XML "
"descriptions. Libglade allow programmers to construct their user interfaces "
"using a graphical interface builder application, and then import those "
"interface definitions."
msgstr ""
"Libglade is 'n biblioteek vir die dinamiese bou van gebruikerkoppelvlakke "
"vanuit XML-beskrywings. Libglade stel programmeerders in staat om hul "
"gebruikerkoppelvlakke te bou met 'n grafiese program vir "
"koppelvlakkonstruksie, en dan hierdie koppelvlakdefinisies bloot in te voer."

#: ../data/overlay.xml.in.h:33
msgid "Library for rendering of SVG vector graphics."
msgstr "Biblioteek vir die verbeelding van SVG-vektorgrafika."

#: ../data/overlay.xml.in.h:34
msgid ""
"Lockdown is the mechanism which is used to bar users using a computing "
"environnment from performing certain actions (like, for instance, printing "
"files, or saving files to disk). The GNOME Desktop already has lockdown "
"support in a few areas (especially the panel and the epiphany web browser). "
"This document aims to cover all the lockdown features found in GNOME, as "
"well as act as a guide on how to preconfigure the desktop settings."
msgstr ""
"Lockdown is die meganisme wat gebruik word om gebruikers in 'n spesifieke "
"omgewing te weerhou van sekere take (soos bv. om lêers te druk of na die "
"skyf te stoor). Die GNOME-werkskerm het reeds het reeds ondersteuning vir "
"sulke beskerming in 'n paar areas (veral die paneel en die webblaaier "
"Epiphany). Hierdie dokument poog om al hierdie beskermingsfunksies in GNOME "
"te dokumenteer, en ook 'n gids te wees vir die voorafopstelling van die "
"werkskerm se instellings."

#: ../data/overlay.xml.in.h:35
msgid "Menu Specification"
msgstr "Kieslysspesifikasie"

#: ../data/overlay.xml.in.h:36
msgid "Nautilus Internals"
msgstr "Interne werking van Nautilus"

#: ../data/overlay.xml.in.h:37
msgid ""
"Nautilus is the official file manager and desktop shell for the GNOME "
"desktop. This paper gives an overview of the design and implementation of "
"Nautilus and some parts of the GNOME platform that it relies on. It also has "
"concrete pointers to interesting parts of the sources for developers who "
"wish to hack on Nautilus, or just learn more about it. This paper requires a "
"basic understanding of how Nautilus works from the user side, and basic "
"knowledge of GNOME programming."
msgstr ""
"Nautilus is die amptelike lêerbestuurder en werkskermdop vir die GNOME-"
"werkskerm. Hierdie artikel gee 'n oorsig oor die ontwerp en implementasie "
"van Nautilus en sekere dele van die GNOME-platform waarop dit staatmaak. Dit "
"het ook konkrete verwysings na interessante dele van die bronkode vir "
"programmeerders wat aan Nautilus wil werk, of net meer daaroor wil uitvind. "
"Hierdie artikel vereis 'n basiese begrip van Nautilus gebruik, en basiese "
"begrip van GNOME-programmering."

#: ../data/overlay.xml.in.h:38
msgid ""
"ORBit is a fast and lightweight CORBA server. GNOME's component "
"architecture, Bonobo, is built on top of CORBA."
msgstr ""
"ORBit is 'n vinnige en liggewig CORBA-bediener. GNOME se "
"komponentargitektuur, Bonobo, is op CORBA gebou."

#: ../data/overlay.xml.in.h:39
msgid "Other related libraries"
msgstr "Ander verwante biblioteke"

#: ../data/overlay.xml.in.h:40
msgid ""
"Pango is the core text and font handling library used in GNOME applications. "
"It has extensive support for the different writing systems used throughout "
"the world."
msgstr ""
"Pango is die hoofbiblioteek vir die hantering vir teks en skriftipes in "
"GNOME-programme. Dit het uitgebreide ondersteuning vir die verskillende "
"skryfstelsels wat regoor die wêreld gebruik word."

#: ../data/overlay.xml.in.h:41
msgid "Plugins for GNOME Applications"
msgstr "Inproppe vir GNOME-programme"

#: ../data/overlay.xml.in.h:42
msgid "Powerful and feature complete XML handling library."
msgstr "Kragtige en funksievolledige biblioteek vir XML-hantering."

#: ../data/overlay.xml.in.h:43
msgid ""
"Powerful framework for creating multimedia applications. Supports both Audio "
"and Video."
msgstr ""
"Kragtige raamwerk vir die skep van multimediaprogramme. Oudio en video word "
"ondersteun."

#: ../data/overlay.xml.in.h:44
msgid "PyGObject Reference Manual"
msgstr "PyGObject-verwysingshandleiding"

#: ../data/overlay.xml.in.h:45
msgid "PyGTK Reference Manual"
msgstr "PyGTK-verwysingshandleiding"

#: ../data/overlay.xml.in.h:46
msgid "References"
msgstr "Verwysings"

#: ../data/overlay.xml.in.h:47
msgid "Shared MIME-info Database Specification"
msgstr "Spesifikasie vir die gedeelde MIME-databasis"

#: ../data/overlay.xml.in.h:48
msgid ""
"Structured File Library (GSF) is an I/O abstraction for reading/writing "
"compound files."
msgstr ""
"Structured File Library (GSF) is 'n Toevoer-/afvoer-abstraksie vir die lees "
"en skryf van saamgestelde lêers."

#: ../data/overlay.xml.in.h:49
msgid "Telepathy Stack"
msgstr ""

#: ../data/overlay.xml.in.h:50
msgid "Terminal emulator widget used by GNOME terminal."
msgstr "Terminaalnabootskomponent gebruik deur GNOME-terminaal."

#: ../data/overlay.xml.in.h:51
msgid ""
"The AT-SPI library provides interfaces which are used by accessibility "
"technologies."
msgstr ""
"Die AT-SPI-biblioteek verskaf koppelvlakke wat deur "
"toeganklikheidstegnologie gebruik word."

#: ../data/overlay.xml.in.h:52
msgid ""
"The Bonobo UI library provides a number of user interface controls using the "
"Bonobo component framework."
msgstr ""
"Die Bonobo-koppelvlakbiblioteek verskaf 'n aantal "
"gebruikerkoppelvlakkontroles wat die Bonobo-komponentraamwerk gebruik."

#: ../data/overlay.xml.in.h:53
msgid ""
"The Desktop Entries provide information about an application such as the "
"name, icon, and description. These files are used for application launchers "
"and for creating menus of applications that can be launched."
msgstr ""
"Die Desktop-inskrywings verskaf inligting oor 'n toepassing, soos die naam, "
"ikoon en beskrywing. Hierdie lêers word gebruik vir toepassinglanseerders en "
"die skep van kieslyste van programme wat begin kan word."

#: ../data/overlay.xml.in.h:54
msgid ""
"The Easy Publish and Consume library (libepc) provides an easy method to "
"publish data using HTTPS, announce that information via DNS-SD, find that "
"information and finally consume it."
msgstr ""
"Die Easy Publish and Consume-biblioteek (libepc) verskaf 'n maklike metode "
"om data te publiseer d.m.v. HTTPS, die data aan te kondig via DNS-SD, die "
"inligting te vind en laastens om dit te verbruik."

#: ../data/overlay.xml.in.h:55
msgid ""
"The GNOME developer suite is a set of tools to ease the life of developers; "
"it features a graphical interface builder, an integrated help system for API "
"reference and more."
msgstr ""
"Die GNOME-ontwikkelingsuite is 'n stel gereedskap om programmering te "
"vergemaklik. Dit bevat 'n grafiese program vir koppelvlakkonstruksie, 'n "
"geïntegreerde hulpstelsel vir API-verwysing, en nogwat."

#: ../data/overlay.xml.in.h:56
msgid ""
"The GnomeCanvas widget provides a flexible widget for creating interactive "
"structured graphics."
msgstr ""
"Die GnomeCanvas-komponent verskaf 'n aanpasbare komponent vir die skep van "
"interaktiewe gestruktureerde grafika."

#: ../data/overlay.xml.in.h:57
msgid ""
"The XSLT C library developed for the Gnome project. XSLT itself is a an XML "
"language to define transformation for XML. Libxslt is based on libxml2."
msgstr ""
"Die XSLT C-biblioteek geskep vir die GNOME-projek. XSLTself is 'n XML-taal "
"vir die definiëring van XML-omskakeling. Libxslt is gebaseer op libxml2."

#: ../data/overlay.xml.in.h:58
msgid ""
"The guides provide the common practices used in code and interface design "
"within the GNOME platform as well as detailed knowledge about some "
"applications and components."
msgstr ""
"Die gidse verskaf die algemene praktyk in die kode en koppelvlakontwerp in "
"die GNOME-platform, asook gedetailleerde inligting aangaande sekere "
"programme en komponente."

#: ../data/overlay.xml.in.h:59
msgid ""
"The libgnome library provides a number of useful routines for building "
"modern applications, including session management, activation of files and "
"URIs, and displaying help."
msgstr ""
"Die libgnome-biblioteek verskaf 'n aantal nuttige roetines vir die bou van "
"moderne toepassings, insluitend sessiebestuur, aktivering van lêers en "
"URI's, en die vertoon van hulp."

#: ../data/overlay.xml.in.h:60
msgid ""
"The libgnomeui library provides additional widgets for applications. Many of "
"the widgets from libgnomeui have already been ported to GTK+."
msgstr ""
"Die libgnomeui-biblioteek verskaf addisionele komponente vir toepassings. "
"Baie van die komponente van libgnomeui is reeds oorgedra na GTK+."

#: ../data/overlay.xml.in.h:61
msgid ""
"The references contain the Application Programming Interface, list of "
"functions, classes and methods of the GNOME platform libraries and the "
"standards used within the GNOME platform."
msgstr ""
"Die verwysings bevat die toepassingsprogrammeringskoppelvlak (API), 'n lys "
"funksies, klasse en metodes van die GNOME-platformbiblioteke en die "
"standaarde wat in die GNOME-platform gebruik word."

#: ../data/overlay.xml.in.h:62
msgid "This document tells you how to make GNOME applications accessible."
msgstr ""
"Hierdie dokument verduidelik hoe om GNOME-programme toeganklik te maak."

#: ../data/overlay.xml.in.h:63
msgid ""
"This freedesktop.org specification attempts to unify the MIME database "
"systems currently in use by X desktop environments."
msgstr ""
"Hierdie freedesktop.org-spesifikasie poog om die MIME-databasisstelsels te "
"verenig wat tans gebruik word op X-werksermomgewings."

#: ../data/overlay.xml.in.h:64
msgid ""
"This freedesktop.org specification describes a common way to name icons and "
"their contexts in an icon theme."
msgstr ""
"Hierdie freedesktop.org-spesifikasie beskryf 'n gemeenskaplike naamkonfensie "
"vir ikone en hul kontekste in 'n ikoontema."

#: ../data/overlay.xml.in.h:65
msgid ""
"This freedesktop.org specification describes a common way to store icon "
"themes."
msgstr ""
"Hierdie freedesktop.org-spesifikasie beskryf 'n gemeenskaplike manier om "
"ikoontemas te stoor."

#: ../data/overlay.xml.in.h:66
msgid ""
"This freedesktop.org specification describes how applications can be started "
"automatically after the user has logged in and how media can request a "
"specific application to be executed or a specific file on the media to be "
"opened after the media has been mounted."
msgstr ""
"Hierdie freedesktop.org-spesifikasie beskryf hoe programme outomaties begin "
"kan word n die gebruiker aangemeld het en hoe media kan aanvra dat 'n "
"spesifieke program uitgevoer word of 'n spesifieke lêer op die media "
"oopgemaak word n die media geheg is."

#: ../data/overlay.xml.in.h:67
msgid ""
"This freedesktop.org specification describes how menus are built up from "
"desktop entries."
msgstr ""
"Hierdie freedesktop.org-spesifikasie beskryf hoe kieslyste gebou word vanuit "
"desktop-inskrywings."

#: ../data/overlay.xml.in.h:68
msgid ""
"This freedesktop.org specification standardizes extensions to the ICCCM "
"between X desktops."
msgstr ""
"Hierdie freedesktop.org-spesifikasie standaardiseer uitbreidings aan die "
"ICCCM tussen X-werkskerms."

#: ../data/overlay.xml.in.h:69
msgid ""
"Various specifications specify files and file formats. This freedesktop.org "
"specification defines where these files should be looked for by defining one "
"or more base directories relative to which files should be located."
msgstr ""
"Verskeie spesifikasies spesifiseer lêers en lêerformate. Hierdie freedesktop."
"org-spesifikasie definieer waar hierdie lêers gesoek moet word deur een of "
"meer basisgidse te definieer ten opsigte waarvan lêers opgespoor moet word."

#: ../data/overlay.xml.in.h:70
msgid "Writing new-style modules for Deskbar-Applet"
msgstr "Hoe om nuwe-styl modules vir die Deskbar-miniprogram te skryf"

#: ../data/overlay.xml.in.h:71
msgid "XDG Base Directory Specification"
msgstr "Spesifikasie vir XDG-basisgidse"

#: ../data/overlay.xml.in.h:72
msgid "gtkmm Documentation"
msgstr "gtkmm-dokumentasie"

#. URL to *translated* Nautilus Internals whitepaper,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:75
msgid ""
"http://developer.gnome.org/doc/whitepapers/nautilus/nautilus-internals.html"
msgstr "-"

#. URL to *translated* GNOME Accessibility Guide,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:78
msgid "http://developer.gnome.org/projects/gap/guide/gad/"
msgstr "-"

#. URL to *translated* GStreamer API reference,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:81
msgid ""
"http://gstreamer.freedesktop.org/data/doc/gstreamer/stable/gstreamer/html/"
msgstr "-"

#. URL to *translated* version of the Admin Guide to GNOME Lockdown
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:84
msgid ""
"http://sayamindu.randomink.org/soc/deployment_guide/deployment_guide.html"
msgstr "-"

#. URL to *translated* version of the Desktop Application Autostart
#. Specification; set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:87
msgid ""
"http://standards.freedesktop.org/autostart-spec/autostart-spec-latest.html"
msgstr "-"

#. URL to *translated* version of the XDG Base Directory Specification
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:90
msgid "http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html"
msgstr "-"

#. URL to *translated* version of the Desktop Entry Specification
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:93
msgid ""
"http://standards.freedesktop.org/desktop-entry-spec/desktop-entry-spec-"
"latest.html"
msgstr "-"

#. URL to *translated* version of the Icon Naming Specification
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:96
msgid ""
"http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest."
"html"
msgstr "-"

#. URL to *translated* version of the Icon Theme Specification
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:99
msgid ""
"http://standards.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html"
msgstr "-"

#. URL to *translated* version of the Menu Specification
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:102
msgid "http://standards.freedesktop.org/menu-spec/menu-spec-latest.html"
msgstr "-"

#. URL to *translated* version of the Shared MIME-info Specification;
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:105
msgid ""
"http://standards.freedesktop.org/shared-mime-info-spec/shared-mime-info-spec-"
"latest.html"
msgstr "-"

#. URL to *translated* version of the Extended Window Manager Hints
#. Specification; set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:108
msgid "http://standards.freedesktop.org/wm-spec/wm-spec-latest.html"
msgstr "-"

#. URL to *translated* libart API reference,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:111
msgid "http://www.gnome.org/~mathieu/libart/libart.html"
msgstr "-"

#. URL to *translated* version of the Writing new-style modules for
#. Deskbar-Applet document,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:115
msgid "http://www.k-d-w.org/deskbar/new-style_modules.html"
msgstr "-"

#. URL to *translated* libxslt API reference,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:118
msgid "http://xmlsoft.org/XSLT/html/libxslt-lib.html"
msgstr "-"

#. URL to *translated* libxml2 API reference,
#. set to dash ("-") if it has not been translated
#: ../data/overlay.xml.in.h:121
msgid "http://xmlsoft.org/html/libxml-lib.html"
msgstr "-"

#: ../data/overlay.xml.in.h:122
msgid "libart Reference Manual"
msgstr "libart-verwysingshandleiding"

#: ../data/overlay.xml.in.h:123
msgid ""
"libsoup is an HTTP client/server library for GNOME. It uses GObjects and the "
"glib main loop, to integrate well with GNOME applications."
msgstr ""
"libsoup is 'n HTTP-klient/bediener-biblioteek vir GNOME. Dit gebruik "
"GObject'e en die glib-programlus ten einde goed te integreer met GNOME-"
"programme."

#: ../data/overlay.xml.in.h:124
msgid "libxml2 Reference Manual"
msgstr "libxml2-verwysingshandleiding"

#: ../data/overlay.xml.in.h:125
msgid "libxslt Reference Manual"
msgstr "libxslt-verwysingshandleiding"
