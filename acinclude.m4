# redefine standard macros to do nothing, so it is not required to have a
# working C build environment on the server to pass the gettext/intltool
# tests

AC_DEFUN([AC_PROG_CC], [])
AC_DEFUN([AC_PROG_CPP], [])
AC_DEFUN([AC_HEADER_STDC], [])

