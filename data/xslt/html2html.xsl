<?xml version='1.0' encoding='UTF-8'?>
<!--
Copyright (c) 2007 Frederic Peters <fpeters@0d.be>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xmlns="http://www.w3.org/1999/xhtml"
		xsl:exclude-result-prefixes="html"
                version="1.0">

<!-- apply libgo style to HTML files previously produced by gtk-doc -->

  <xsl:import href="heading.xsl"/>

  <xsl:param name="libgo.lang" select="'en'"/>
  <xsl:param name="libgo.channel" select="'devel'"/>
  <xsl:param name="default.encoding" select="'UTF-8'"/>

  <xsl:param name="libgo.extracss"/>
  <xsl:param name="libgo.rewritebase"/>
  <xsl:param name="libgo.originalhref"/>
  <xsl:param name="libgo.h2hmode" select="'body'"/>
  <xsl:param name="libgo.hidehref" select="'false'"/>

  <xsl:output method="html" encoding="UTF-8" indent="yes"
  	omit-xml-declaration="yes"
  	doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
	doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

  <!-- identity -->
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>

  <!-- gtkmm: fix xrefs between glibmm and gtkmm -->
  <xsl:template match="node()|@*" mode="gtkmm">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="gtkmm"/>
    </xsl:copy>
  </xsl:template>

  <!--
  <xsl:template match="@href" mode="gtkmm">
    <xsl:attribute name="href">
      <xsl:choose>
        <xsl:when test="contains(., 'glibmm-2.4/')">
          <xsl:value-of select="concat('../../glibmm/unstable/', substring-after(., 'html/'))"/>
        </xsl:when>
        <xsl:when test="contains(., 'gtkmm-2.4/')">
          <xsl:value-of select="concat('../../gtkmm/unstable/', substring-after(., 'html/'))"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  -->

  <!-- gtk-doc: fix links between gtk-doc references -->
  <xsl:template match="node()|@*" mode="gtk-doc">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*" mode="gtk-doc"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@href" mode="gtk-doc">
   <xsl:attribute name="href">
     <xsl:choose>
       <xsl:when test="starts-with(., 'http://library.gnome.org/devel/') or (not(starts-with(., 'http://')) and not(starts-with(., 'ftp://')))">
         <xsl:choose>
           <xsl:when test="contains(., '/gobject/stable/')">
             <xsl:value-of select="concat('../../gobject/stable/', substring-after(., '/gobject/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gobject/unstable/')">
             <xsl:value-of select="concat('../../gobject/unstable/', substring-after(., '/gobject/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gobject/')">
             <xsl:value-of select="concat('../../gobject/stable/', substring-after(., '/gobject/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/glib/stable/')">
             <xsl:value-of select="concat('../../glib/stable/', substring-after(., '/glib/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/glib/unstable/')">
             <xsl:value-of select="concat('../../glib/unstable/', substring-after(., '/glib/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/glib/')">
             <xsl:value-of select="concat('../../glib/stable/', substring-after(., '/glib/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/atk/unstable/')">
             <xsl:value-of select="concat('../../atk/unstable/', substring-after(., '/atk/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/atk/')">
             <xsl:value-of select="concat('../../atk/stable/', substring-after(., '/atk/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk/stable/')">
             <xsl:value-of select="concat('../../gtk/stable/', substring-after(., '/gtk/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk/unstable/')">
             <xsl:value-of select="concat('../../gtk/unstable/', substring-after(., '/gtk/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk/')">
             <xsl:value-of select="concat('../../gtk/stable/', substring-after(., '/gtk/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk/stable/')">
             <xsl:value-of select="concat('../../gdk/stable/', substring-after(., '/gdk/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk/unstable/')">
             <xsl:value-of select="concat('../../gdk/unstable/', substring-after(., '/gdk/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk/') and $libgo.module = 'gtk3'">
             <xsl:value-of select="concat('../../gdk3/stable/', substring-after(., '/gdk/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk/')">
             <xsl:value-of select="concat('../../gdk/stable/', substring-after(., '/gdk/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/pango/stable/')">
             <xsl:value-of select="concat('../../pango/stable/', substring-after(., '/pango/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/pango/unstable/')">
             <xsl:value-of select="concat('../../pango/unstable/', substring-after(., '/pango/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/pango/')">
             <xsl:value-of select="concat('../../pango/stable/', substring-after(., '/pango/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk-pixbuf/stable/')">
             <xsl:value-of select="concat('../../gdk-pixbuf/stable/', substring-after(., '/gdk-pixbuf/stable'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk-pixbuf/unstable/')">
             <xsl:value-of select="concat('../../gdk-pixbuf/unstable/', substring-after(., '/gdk-pixbuf/unstable'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk-pixbuf/')">
             <xsl:value-of select="concat('../../gdk-pixbuf/stable/', substring-after(., '/gdk-pixbuf/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gio/stable/')">
             <xsl:value-of select="concat('../../gio/stable/', substring-after(., '/gio/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gio/unstable/')">
             <xsl:value-of select="concat('../../gio/unstable/', substring-after(., '/gio/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gio/')">
             <xsl:value-of select="concat('../../gio/stable/', substring-after(., '/gio/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy/stable/')">
             <xsl:value-of select="concat('../../libempathy/stable/', substring-after(., '/libempathy/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy/unstable/')">
             <xsl:value-of select="concat('../../libempathy/unstable/', substring-after(., '/libempathy/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy/')">
             <xsl:value-of select="concat('../../libempathy/stable/', substring-after(., '/libempathy/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy-gtk/stable/')">
             <xsl:value-of select="concat('../../libempathy-gtk/stable/', substring-after(., '/libempathy-gtk/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy-gtk/unstable/')">
             <xsl:value-of select="concat('../../libempathy-gtk/unstable/', substring-after(., '/libempathy-gtk/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libempathy-gtk/')">
             <xsl:value-of select="concat('../../libempathy-gtk/stable/', substring-after(., '/libempathy-gtk/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/libgnome/')">
             <xsl:value-of select="concat('../../libgnome/stable/', substring-after(., '/libgnome/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gconf/')">
             <xsl:value-of select="concat('../../gconf/stable/', substring-after(., '/gconf/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/cairo/stable/')">
             <xsl:value-of select="concat('../../cairo/stable/', substring-after(., '/cairo/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/cairo/unstable/')">
             <xsl:value-of select="concat('../../cairo/unstable/', substring-after(., '/cairo/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/cairo/')">
             <xsl:value-of select="concat('../../cairo/stable/', substring-after(., '/cairo/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk3/stable/')">
             <xsl:value-of select="concat('../../gtk3/stable/', substring-after(., '/gtk3/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk3/unstable/')">
             <xsl:value-of select="concat('../../gtk3/unstable/', substring-after(., '/gtk3/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gtk3/')">
             <xsl:value-of select="concat('../../gtk3/stable/', substring-after(., '/gtk3/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk3/stable/')">
             <xsl:value-of select="concat('../../gdk3/stable/', substring-after(., '/gdk3/stable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk3/unstable/')">
             <xsl:value-of select="concat('../../gdk3/unstable/', substring-after(., '/gdk3/unstable/'))"/>
           </xsl:when>
           <xsl:when test="contains(., '/gdk3/')">
             <xsl:value-of select="concat('../../gdk3/stable/', substring-after(., '/gdk3/'))"/>
           </xsl:when>
           <xsl:otherwise>
             <xsl:value-of select="."/>
           </xsl:otherwise>
         </xsl:choose>
       </xsl:when>
       <xsl:otherwise>
         <xsl:value-of select="."/>
       </xsl:otherwise>
     </xsl:choose>
   </xsl:attribute>
  </xsl:template>

  <xsl:template match="@href">
   <xsl:attribute name="href">
    <xsl:choose>
     <xsl:when test="starts-with(., '/') and $libgo.rewritebase">
       <xsl:value-of select="concat($libgo.rewritebase, .)"/>
     </xsl:when>
     <xsl:otherwise>
       <xsl:value-of select="."/>
     </xsl:otherwise>
    </xsl:choose>
   </xsl:attribute>
  </xsl:template>

  <xsl:template match="meta[@content='text/html; charset=US-ASCII']">
  </xsl:template>

  <xsl:template match="html:html">
    <html>
     <head>
      <meta content="text/html; charset=utf-8" />
      <xsl:if test="$libgo.h2hmode = 'gtkmm'">
        <link rel="stylesheet" href="/skin/doxygen.css" type="text/css" />
      </xsl:if>
      <xsl:call-template name="libgo.head"/>
      <xsl:if test="$libgo.extracss">
        <link rel="stylesheet" href="/skin/{$libgo.extracss}" type="text/css" />
      </xsl:if>
      <xsl:apply-templates select="html:head/html:title"/>
      <xsl:apply-templates select="html:head/html:link[@rel != 'stylesheet']"/>
      <link rel="stylesheet" type="text/css" media="all" href="/skin/jquery.autocomplete.css"/>
      <link rel="stylesheet" type="text/css" media="all" href="/skin/960_24_col.css"/>
      <script type="text/javascript" src="/js/jquery.js"></script>
      <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
      <script type="text/javascript" src="/js/jquery.autocomplete.js"></script>

     </head>
     <body>
      <xsl:call-template name="libgo.header" />
      <div id="container">
        <xsl:attribute name="class">
          <xsl:value-of select="$libgo.h2hmode"/>
          <xsl:if test="//html:div[@class='toc']">
            <xsl:text> two_columns</xsl:text>
          </xsl:if>
        </xsl:attribute>
        <div class="content">
        <xsl:if test="$libgo.originalhref and $libgo.hidehref != 'true'">
          <div class="action_box">
	  <p class="canonical-ref">
          Canonical document reference:
	  <a href="{$libgo.originalhref}"><xsl:value-of select="$libgo.originalhref"/></a>
	  </p>
          </div>
	</xsl:if>
        <xsl:choose>
          <xsl:when test="$libgo.h2hmode = 'gtkmm'">
	    <h1 class="lgotitle"><xsl:value-of select="//html:title" /></h1>
	    <xsl:choose>
	      <!-- Doxygen versions older than 1.6.0? -->
	      <xsl:when test="//html:div[@id='content']">
	        <xsl:apply-templates select="//html:div[@id='content']" mode="gtkmm"/>
	      </xsl:when>
	      <!-- Doxygen versions between 1.6.0 and 1.7.0 -->
	      <xsl:when test="//html:div[@id='navigation']">
	        <div id="content">
	          <xsl:apply-templates select="//html:div[@class='navigation']" mode="gtkmm"/>
	          <xsl:apply-templates select="//html:div[@class='contents']" mode="gtkmm"/>
		</div>
	      </xsl:when>
	      <!-- Doxygen 1.7.0 or greater -->
	      <xsl:when test="//html:div[@id='navrow1']">
	        <div id="content">
		  <xsl:apply-templates select="//html:div[@class='tabs']" mode="gtkmm"/>
		  <xsl:apply-templates select="//html:div[@class='contents']" mode="gtkmm"/>
		</div>
	      </xsl:when>
	    </xsl:choose>
          </xsl:when>
          <xsl:when test="$libgo.h2hmode = 'content-id'">
            <xsl:apply-templates select="//html:div[@id='content']"/>
          </xsl:when>
          <xsl:when test="$libgo.h2hmode = 'gtk-doc'">
            <xsl:apply-templates select="html:body/*" mode="gtk-doc"/>
          </xsl:when>
          <xsl:otherwise> <!-- libgo.h2hmode = 'body' -->
            <xsl:apply-templates select="html:body/*"/>
          </xsl:otherwise>
        </xsl:choose>
        </div>
        <div id="footer_art" class="default"> </div>
      </div>
      <xsl:call-template name="libgo.footer"/>
     </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
