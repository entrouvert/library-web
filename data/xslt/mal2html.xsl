<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2009 Frederic Peters <fpeters@gnome.org>

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:set="http://exslt.org/sets"
                xmlns="http://www.w3.org/1999/xhtml"
                xmlns:str="http://exslt.org/strings"
                xmlns:mal="http://projectmallard.org/1.0/"
                extension-element-prefixes="exsl"
                xsl:exclude-result-prefixes="set str"
                version="1.0">

<!-- FIXME: Shaun's styles are under LGPL, is it OK to use GPL for this? -->
<xsl:import href="/usr/share/yelp-xsl/xslt/mallard/html/mal2html.xsl"/>
<xsl:import href="heading.xsl"/>
<xsl:import href="gettext.xsl"/>

<!-- Setting parameters for included stylesheets -->
<xsl:param name="mal.link.extension" select="concat('.html.',$libgo.lang)"/>
<xsl:param name="html.extension" select="concat('.html.',$libgo.lang)"/>

<xsl:param name="mal.cache" select="document($mal.cache.file, /)/mal:cache"/>

<!-- output -->
<xsl:output method="html" encoding="UTF-8" indent="yes"
        omit-xml-declaration="yes"
	doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
	doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<!-- This gets set on the command line ... -->
<xsl:param name="libgo.lang" select="''"/>
<xsl:param name="libgo.tarball" select="false()"/>
<xsl:param name="libgo.languages_in_sidebar" select="false()"/>
<xsl:param name="libgo.nightly" select="false()"/>

<!-- Setting parameters for included stylesheets -->
<xsl:param name="theme.icon.admon.path" select="'/skin/'"/>
<xsl:param name="theme.icon.nav.previous" select="'/skin/nav-previous.png'"/>
<xsl:param name="theme.icon.nav.next" select="'/skin/nav-next.png'"/>

<!-- Process -->

<!-- == html.output == -->
<xsl:template name="html.output">
  <xsl:param name="node" select="."/>
  <xsl:param name="href">
    <xsl:choose>
      <xsl:when test="$node/@xml:id">
        <xsl:value-of select="$node/@xml:id"/>
      </xsl:when>
      <xsl:when test="$node/@id">
        <xsl:value-of select="$node/@id"/>
      </xsl:when>
      <xsl:when test="set:has-same-node($node, /*)">
        <xsl:value-of select="$html.basename"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="generate-id()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:param>
  <exsl:document href="{$libgo.mallard.html.basedir}/{$href}{$html.extension}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
    <xsl:call-template name="html.page">
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
  </exsl:document>
  <xsl:apply-templates mode="html.output.after.mode" select="$node"/>
</xsl:template>

<xsl:template name="html.page">
  <xsl:param name="node" select="."/>
  <html>
    <head>
      <title>
        <xsl:apply-templates mode="html.title.mode" select="$node"/>
      </title>
      <xsl:call-template name="html.head.custom"/>
      <xsl:call-template name="html.css"/>
      <xsl:call-template name="libgo.head"/>
      <script type="text/javascript" src="/js/jquery.js" />
      <link rel="stylesheet" type="text/css" href="/skin/mallard.css"/>
      <xsl:call-template name="mal2html.facets.js"/>
       <script type="text/javascript" language="javascript"><xsl:text><![CDATA[
$(document).ready(function () {
  $('div.mouseovers').each(function () {
    var contdiv = $(this);
    var width = 0;
    var height = 0;
    var img = null;
    contdiv.find('img').each(function () {
      img = $(this);
      if ($(this).attr('data-yelp-match') == '')
        $(this).show();
    });
    contdiv.next('ul').find('a').each(function () {
      var mlink = $(this);
      console.log('mlink', $(this));
      mlink.hover(
        function () {
          var offset = img.offset();
          mlink.find('img').css({left: offset.left, top: offset.top, zIndex: 10});
          mlink.find('img').fadeIn('fast');
        },
        function () {
          mlink.find('img').fadeOut('fast');
        }
      );
    });
  })
});
       ]]></xsl:text></script>
    </head>
    <body>
      <xsl:apply-templates mode="html.body.attr.mode" select="$node"/>
      <xsl:call-template name="libgo.header">
        <xsl:with-param name="lang"><xsl:value-of select="$libgo.lang"/></xsl:with-param>
      </xsl:call-template>
      <div id="container">
      <div class="head">
        <xsl:apply-templates mode="html.header.mode" select="$node"/>
      </div>
      <div class="body">
        <xsl:apply-templates mode="html.body.mode" select="$node"/>
      </div>
      <div id="footer_art" class="default"> </div>
      </div>
      <xsl:call-template name="libgo.footer"/>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
