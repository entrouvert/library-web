<?xml version='1.0' encoding='UTF-8'?>
<!--
Copyright (c) 2007 Frederic Peters <fpeters@0d.be>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                extension-element-prefixes="exsl"
                version="1.0">

  <xsl:param name="gduxrefs" select="document('../tmp/gduxrefs.xml')/gduxrefs" />

  <xsl:template name="libgo.xref" match="@url">
    <xsl:param name="module">
      <xsl:choose>
        <xsl:when test="substring-before(substring-after(., 'ghelp:'), '?')">
          <xsl:value-of select="substring-before(substring-after(., 'ghelp:'), '?')"/>
        </xsl:when>
        <xsl:when test="substring-before(substring-after(., 'ghelp:'), '#')">
          <xsl:value-of select="substring-before(substring-after(., 'ghelp:'), '#')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="substring-after(., 'ghelp:')"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="page">
      <xsl:choose>
        <xsl:when test="substring-after(., '?')">
          <xsl:value-of select="substring-after(., '?')"/>
        </xsl:when>
        <xsl:when test="substring-after(., '#')">
          <xsl:value-of select="substring-after(., '#')"/>
	</xsl:when>
      </xsl:choose>
    </xsl:param>
    <xsl:param name="path" select="$gduxrefs/doc[@id = $module]/@path" />

    <xsl:choose>
      <xsl:when test="$path"><xsl:value-of select="$path"/><xsl:if test="$page"
        ><xsl:value-of select="$page"/></xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>X: Failed to find gnome-doc-utils cross-reference: <xsl:value-of
                select="."/> (module: <xsl:value-of select="$module"/>)</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
