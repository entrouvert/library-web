<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

<xsl:import href="gettext.xsl"/>

<xsl:param name="libgo.channel">undefined</xsl:param>
<xsl:param name="libgo.dbm_support" select="false()"/>

<xsl:template name="libgo.header">
  <xsl:param name="channel"><xsl:value-of select="$libgo.channel"/></xsl:param>
  <xsl:param name="lang"><xsl:value-of select="$libgo.lang"/></xsl:param>

	<div class="page-wrapper">
	<h1 id="logo"><a href="/"><img src="http://www.entrouvert.com/static/eo/img/logo.png" alt="Entr'ouvert"/></a> Entr'ouvert Documentation</h1>
	</div>


    <div class="clearfix"></div>
</xsl:template>

<xsl:template name="libgo.head">
  <link rel="stylesheet" type="text/css" media="all"
        href="http://www.entrouvert.com/static/eo/css/eo.css" />
  <link rel="stylesheet" type="text/css" media="all" href="/skin/eo.css"/>
</xsl:template>

<xsl:template name="libgo.footer">
    <div class="clearfix"></div>
    <div id="footer">

    </div>
</xsl:template>




</xsl:stylesheet>
