<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2006 Goran Rakic <grakic@devbase.net>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                extension-element-prefixes="exsl"
                version="1.0">


<xsl:import href="gettext.xsl"/>
<xsl:import href="heading.xsl"/>

<!-- This gets set on the command line ... -->
<xsl:param name="libgo.lang" select="''"/>
<xsl:param name="libgo.debug" select="false()"/>

<xsl:variable name="lcletters">abcdefghijklmnopqrstuvwxyz</xsl:variable>
<xsl:variable name="ucletters">ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:variable>


<xsl:output method="html" encoding="UTF-8" indent="yes"
omit-xml-declaration="yes"
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<xsl:namespace-alias stylesheet-prefix="html" result-prefix="#default"/>

<xsl:template name="footer">
<div id="footer">
<p>
Copyright © 2012 <a href="http://www.entrouvert.com/">Entr'ouvert</a>.
<br />
Hosted documents have their own copyright notices.
</p>
</div>
</xsl:template>

<xsl:template name="category-title">
<xsl:param name="lang"/>
<xsl:param name="tocid"/>

<xsl:variable name="rtocid">
<xsl:choose>
<xsl:when test="document('../externals/toc.xml')//toc[@id = $tocid]">
  <xsl:value-of select="$tocid"/>
</xsl:when>
<!-- Rarian 0.8 changed all toc id; as library still needs to handle
     older documents, here is a quick mapping -->
<xsl:when test="$tocid = 'index'">Core</xsl:when>
<xsl:when test="$tocid = 'ApplicationsAccessibility'">Accessibility</xsl:when>
<xsl:when test="$tocid = 'ApplicationsAccessories'">Utility</xsl:when>
<xsl:when test="$tocid = 'ApplicationsEducation'">Education</xsl:when>
<xsl:when test="$tocid = 'ApplicationsGames'">Game</xsl:when>
<xsl:when test="$tocid = 'ApplicationsGraphics'">Graphics</xsl:when>
<xsl:when test="$tocid = 'ApplicationsInternet'">Network</xsl:when>
<xsl:when test="$tocid = 'ApplicationsOffice'">Office</xsl:when>
<xsl:when test="$tocid = 'ApplicationsOther'">Other</xsl:when>
<xsl:when test="$tocid = 'DesktopApplets'">Other</xsl:when>
<xsl:when test="$tocid = 'ApplicationsScientific'">Science</xsl:when>
<xsl:when test="$tocid = 'ApplicationsMultimedia'">AudioVideo</xsl:when>
<xsl:when test="$tocid = 'ApplicationsSystem'">System</xsl:when>
</xsl:choose>
</xsl:variable>

<xsl:choose>
<xsl:when test="document('../externals/toc.xml')//toc[@id = $rtocid]/title">
<xsl:variable name="cat-icon"
	select="document('../externals/toc.xml')//toc[@id = $rtocid]/@icon"/>
<h2>
  <xsl:attribute name="class">category<xsl:if test="$cat-icon != ''"
	> cat-<xsl:value-of select="$cat-icon"/></xsl:if></xsl:attribute>
  <xsl:if test="$cat-icon != ''">
    <xsl:attribute name="id"><xsl:value-of select="$cat-icon"/></xsl:attribute>
  </xsl:if>
<xsl:choose>
  <xsl:when test="document('../externals/toc.xml')//toc[@id = $rtocid]/title[@xml:lang = $lang]">
    <xsl:value-of select="document('../externals/toc.xml')//toc[@id = $rtocid]/title[@xml:lang = $lang]"/>
  </xsl:when>
  <xsl:otherwise>
    <xsl:value-of select="document('../externals/toc.xml')//toc[@id = $rtocid]/title"/>
  </xsl:otherwise>
</xsl:choose>
</h2>
</xsl:when>
<xsl:otherwise>
<h2 class="category cat-{$tocid}" id="{$tocid}">
  <xsl:call-template name="gettext"><xsl:with-param name="lang"
    select="$lang"/><xsl:with-param name="msgid"
    select="$tocid"/></xsl:call-template>
</h2>
</xsl:otherwise>
</xsl:choose>

</xsl:template>


<xsl:template name="language-label">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="document('../languages.xml')//lang[@code = $lang]">
<xsl:value-of select="document('../languages.xml')//lang[@code =
$lang]"/>
</xsl:when>
<xsl:when test="document('../languages.xml')//lang[substring(@code, 1, 2) = $lang]">
<xsl:value-of
select="document('../languages.xml')//lang[substring(@code, 1, 2) =
$lang]"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$lang"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="subsection" mode="title">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="title[@xml:lang = $lang]">
<xsl:value-of select="title[@xml:lang = $lang]"/>
</xsl:when>
<xsl:otherwise><xsl:value-of select="title"/></xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="subsection" mode="intro">
<xsl:param name="lang"/>
<xsl:if test="intro">
<p>
<xsl:choose>
<xsl:when test="intro[@xml:lang = $lang]">
  <xsl:value-of select="intro[@xml:lang = $lang]"/>
</xsl:when>
<xsl:otherwise><xsl:value-of select="intro"/></xsl:otherwise>
</xsl:choose>
</p>
</xsl:if>
</xsl:template>



<xsl:template name="overlay-section-title">
<xsl:param name="lang"/>
<xsl:choose>
<xsl:when test="document('../overlay.xml')//subsection[@code = $lang]">
<xsl:value-of select="document('../languages.xml')//lang[@code =
$lang]"/>
</xsl:when>
<xsl:when test="document('../languages.xml')//lang[substring(@code, 1, 2) = $lang]">
<xsl:value-of
select="document('../languages.xml')//lang[substring(@code, 1, 2) =
$lang]"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="$lang"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template>



<xsl:template match="document" mode="channelindex">
<xsl:param name="ignoredeprecated" select="true()"/>
<xsl:param name="lang"/>
<xsl:if test="not($ignoredeprecated) or not(keywords/keyword[. = 'upcoming-deprecation'])">
<dt>
<xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
<xsl:attribute name="class">upcoming-deprecation</xsl:attribute>
</xsl:if>
<a lang="{@lang}">
<xsl:attribute name="href">
  <xsl:if test="@path"><xsl:value-of select="@path"/></xsl:if>
  <xsl:if test="@href"><xsl:value-of select="@href"/></xsl:if>
</xsl:attribute>
<xsl:if test="@href">
  <xsl:attribute name="class">external</xsl:attribute>
</xsl:if>
<xsl:choose>
<xsl:when test="normalize-space(title)">
  <xsl:value-of select="title" />
</xsl:when>
<xsl:otherwise>
  <xsl:value-of select="@module" />
</xsl:otherwise>
</xsl:choose>
</a>
<xsl:if test="@path and versions/version">
<xsl:text> </xsl:text>
<span class="module-more">[<a href="{@modulename}/"><xsl:call-template
    name="gettext"><xsl:with-param name="lang"
    select="@lang"/><xsl:with-param name="msgid"
    select="'more-versions-languages-or-options'"/></xsl:call-template></a>]</span>
</xsl:if>
<xsl:if test="@href">
<xsl:text> </xsl:text>
<span class="module-more">[<xsl:call-template
    name="gettext"><xsl:with-param name="lang"
    select="$lang"/><xsl:with-param name="msgid"
    select="'external-resource'"/></xsl:call-template>]</span>
</xsl:if>
</dt>

<dd>
<xsl:if test="abstract">
<p><xsl:value-of select="abstract" /></p>
</xsl:if>
<xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
<p class="upcoming-deprecation">
 <xsl:call-template name="gettext"><xsl:with-param name="lang"
      select="$lang"/><xsl:with-param name="msgid"
      select="'upcoming-deprecation'"/></xsl:call-template>
</p>
</xsl:if>
<xsl:if test="count(versions/version) > 1 and versions/version[@keyword = 'unstable']">
<p class="other-versions">
 <xsl:call-template name="gettext"><xsl:with-param name="lang"
      select="$lang"/><xsl:with-param name="msgid"
      select="'see-also'"/></xsl:call-template><xsl:text> </xsl:text>
  <a href="{@modulename}/unstable/" lang="{@lang}"><xsl:call-template
    name="gettext"><xsl:with-param name="lang"
      select="$lang"/><xsl:with-param name="msgid"
      select="'development-version-doc'"/></xsl:call-template></a>
</p>
</xsl:if>
</dd>
</xsl:if>
</xsl:template>

<xsl:template match="document[@path != '' and (not(@single) or @single != 'true')]" mode="modindex">
<xsl:param name="lang"/>
<xsl:if test="concat('/', @channel, '/', @modulename, '/') != @path">
<!-- don't write document index if the document is not versioned -->
<xsl:variable name="modulename" select="@modulename"/>

<xsl:if test="$libgo.debug">
<xsl:message>Writing <xsl:value-of
	select="concat(@modulename, '/index.html.', $lang)" /></xsl:message>
</xsl:if>

<exsl:document href="{@modulename}/index.html.{$lang}"
method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
doctype-system="http://www.w3.org/TR/html4/loose.dtd">
<html lang="{$lang}">
<head>
  <title><xsl:value-of select="title" /> - 
	  <xsl:call-template name="gettext"><xsl:with-param name="lang"
	  select="$lang"/><xsl:with-param name="msgid"
	  select="'gnome-developer-center'"/></xsl:call-template></title>
  <xsl:call-template name="libgo.head">
    <xsl:with-param name="channel" select="@channel"/>
  </xsl:call-template>
  <script type="text/javascript" src="/js/strings.js" />
</head>
<body class="with-star">
  <xsl:call-template name="libgo.header">
    <xsl:with-param name="channel">devel</xsl:with-param>
    <xsl:with-param name="lang" select="$lang"/>
  </xsl:call-template>
  <div id="container" class="page-wrapper">
    <div class="container_12">
    <div class="page_title">
      <h1 class="article title"><a href="{@path}"
          lang="{@lang}"><xsl:value-of select="title"/></a></h1>
    </div>
    <div class="content">
	    <xsl:if test="abstract">
	    <p>
	     <xsl:value-of select="abstract" />
	    </p>
	    </xsl:if>

	    <xsl:if test="keywords/keyword[. = 'upcoming-deprecation']">
	      <p class="upcoming-deprecation">
	        <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'upcoming-deprecation'"/></xsl:call-template>
	      </p>
	    </xsl:if>
	    <xsl:if test="versions">
	      <h4 class="versions">
              <xsl:call-template name="gettext"><xsl:with-param name="lang"
              select="$lang"/><xsl:with-param name="msgid"
              select="'availableversions'"/></xsl:call-template>
	      </h4>
	      <ul class="versions">
	      <xsl:for-each select="versions/version">
	        <li>
		 <xsl:choose>
		   <xsl:when test="@keyword = 'stable'"><strong>
		    <a href="{@href}/" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		    </strong>
		   </xsl:when>
		   <xsl:otherwise>
		    <a href="{@href}/" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		   </xsl:otherwise>
		 </xsl:choose>
		 <xsl:if test="@keyword = 'unstable'">
		    (<xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'development-version'"/></xsl:call-template>)
		 </xsl:if>
		</li>
	      </xsl:for-each>
	      </ul>

	      <xsl:if test="@single_page_alternative = 'true'">
	      <h4><xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'allinonepage'"/></xsl:call-template></h4>
	      <ul class="versions">
	      <xsl:for-each select="versions/version">
	        <li>
		 <xsl:choose>
		   <xsl:when test="@keyword = 'stable'"><strong>
		     <a href="{@href}/{$modulename}.html" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		    </strong>
		   </xsl:when>
		   <xsl:otherwise>
		     <a href="{@href}/{$modulename}.html" lang="{@lang}"><xsl:apply-templates
		 	select="." mode="version-name"><xsl:with-param name="lang" select="$lang"/>
			</xsl:apply-templates></a>
		   </xsl:otherwise>
		 </xsl:choose>
		 <xsl:if test="@keyword = 'unstable'">
		    (<xsl:call-template name="gettext"><xsl:with-param name="lang"
		      select="$lang"/><xsl:with-param name="msgid"
		      select="'development-version'"/></xsl:call-template>)
		 </xsl:if>
		</li>
	      </xsl:for-each>
	      </ul>
	      </xsl:if>
	    </xsl:if>

	  </div>
	  <div class="sidebar">
	    <xsl:if test="tarballs">
            <div class="downloads subtle_box">
            <h4>
              <xsl:call-template name="gettext"><xsl:with-param name="lang"
              select="$lang"/><xsl:with-param name="msgid"
              select="'downloads'"/></xsl:call-template>
            </h4>
	    <ul>
	      <xsl:for-each select="tarballs/tarball">
                <li><a href="{text()}"><xsl:value-of select="text()"/></a></li>
              </xsl:for-each>
	    </ul>
	    <xsl:if test="keywords/keyword[. = 'gtk-doc']">
	      <p class="devhelp-note">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'devhelp-note'"/></xsl:call-template>
	      </p>
	    </xsl:if>
            </div>
	    </xsl:if>

            <xsl:if test="other-languages/lang">
            <div class="other-languages subtle_box">
              <script type="text/javascript" src="/js/language.js" />
              <script type="text/javascript">display_missing_translation_text()</script>
            </div>
            </xsl:if>

            <xsl:if test="@tarballname">
            <div class="tarballname subtle_box">
              <p>
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="$lang"/><xsl:with-param name="msgid"
		  select="'tarball-location'"/></xsl:call-template>
                <xsl:text> </xsl:text>
                <xsl:value-of select="@tarballname"/>
              </p>
            </div>
            </xsl:if>

	    </div>
	  </div>
	  <div id="footer_art" class="default"> </div>
          </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>
    </xsl:if>
  </xsl:template>

  <xsl:template match="version" mode="version-name">
    <xsl:param name="lang"/>
    <xsl:choose>
      <xsl:when test=". = 'nightly'">
        <xsl:call-template name="gettext"><xsl:with-param name="lang"
          select="$lang"/><xsl:with-param name="msgid"
	  select="'nightly-version'"/></xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="section" mode="channelindex">
    <xsl:param name="lang"/>
    <xsl:param name="hidetitle" value="false()"/>

    <xsl:if test="not($hidetitle)">
    <xsl:call-template name="category-title">
      <xsl:with-param name="lang" select="$lang"/>
      <xsl:with-param name="tocid" select="@toc_id"/>
    </xsl:call-template>
    </xsl:if>

    <xsl:for-each select="section">
      <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
      <xsl:variable name="title" select="@title"/>
      <xsl:if test="document">
        <h3 class="subsection" id="{@title}">
          <xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
                             mode="title">
            <xsl:with-param name="lang" select="$lang" />
          </xsl:apply-templates>
        </h3>
        <xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
                           mode="intro">
          <xsl:with-param name="lang" select="$lang" />
        </xsl:apply-templates>
        <dl class="doc-index">
        <xsl:for-each select="document">
          <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
          <xsl:sort select="translate(title, $ucletters, $lcletters)"/>
          <xsl:apply-templates select="." mode="channelindex">
            <xsl:with-param name="lang" select="$lang"/>
            <xsl:with-param name="ignoredeprecated" select="true()"/>
  	</xsl:apply-templates>
        </xsl:for-each>
        </dl>
      </xsl:if>
    </xsl:for-each>

    <xsl:if test="section and document">
      <h3 class="subsection">
        <xsl:call-template name="gettext">
          <xsl:with-param name="lang" select="$lang"/>
          <xsl:with-param name="msgid" select="'others'"/>
        </xsl:call-template>
      </h3>
    </xsl:if>

    <dl class="doc-index">
      <xsl:for-each select="document">
        <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
        <xsl:sort select="translate(title, $ucletters, $lcletters)"/>
        <xsl:apply-templates select="." mode="channelindex">
          <xsl:with-param name="lang" select="$lang"/>
	</xsl:apply-templates>
      </xsl:for-each>
    </dl>
  </xsl:template>

  <xsl:template match="index" mode="channelindex">
    <div class="subindex" id="subindex-{@id}">
      <h2><a href="{@id}"><xsl:value-of select="title"/></a></h2>
      <xsl:if test="abstract">
        <p><xsl:value-of select="abstract"/></p>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:template match="index" mode="toc">
  <xsl:param name="lang" value="@lang"/>

  <ul class="indextoc">
   <xsl:for-each select="section">
     <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
     <li>
      <a href="#{@toc_id}"><xsl:call-template name="gettext"><xsl:with-param name="lang"
        select="$lang"/><xsl:with-param name="msgid"
        select="@toc_id"/></xsl:call-template></a>
     <xsl:if test="section">
      <ul>
       <xsl:for-each select="section">
        <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
        <xsl:if test="document">
          <li>
            <xsl:variable name="title" select="@title"/>
            <a href="#{@title}"><xsl:apply-templates select="document('../overlay.xml')//subsection[@id = $title]"
              mode="title">
             <xsl:with-param name="lang" select="$lang" />
             </xsl:apply-templates></a>
          </li>
        </xsl:if>
       </xsl:for-each>
      </ul>
      </xsl:if>
      </li>
     </xsl:for-each>
     <xsl:if test="//keywords/keyword[. = 'upcoming-deprecation']">
     <li><a href="deprecated">Deprecated APIs</a></li>
     </xsl:if>
    </ul>
  </xsl:template>

  <xsl:template match="index">
    <xsl:param name="channel" select="@channel"/>
    <xsl:param name="lang" select="@lang"/>
    <xsl:param name="filename">
      <xsl:choose>
        <xsl:when test="@id"><xsl:value-of select="@id"/></xsl:when>
        <xsl:otherwise>more</xsl:otherwise>
      </xsl:choose>
    </xsl:param>

    <xsl:if test="$libgo.debug">
      <xsl:message>Writing channel: <xsl:value-of
                select="concat($filename, '.html.', @lang)" /></xsl:message>
    </xsl:if>

    <xsl:apply-templates select="index" />

    <exsl:document href="{$filename}.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
	<head>
	  <title>
            <xsl:choose>
	      <xsl:when test="@channel = 'users'">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'userslabel'"/></xsl:call-template>
	  	-
		<xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="@lang"/><xsl:with-param name="msgid"
		  select="'gnome-developer-center'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:when test="@channel = 'devel'">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'developerslabel'"/></xsl:call-template>
		-
		<xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="@lang"/><xsl:with-param name="msgid"
		  select="'gnome-developer-center'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:when test="@channel = 'admin'">
                <xsl:call-template name="gettext"><xsl:with-param name="lang"
                select="@lang"/><xsl:with-param name="msgid"
                select="'sysadminslabel'"/></xsl:call-template>
		-
		<xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="@lang"/><xsl:with-param name="msgid"
		  select="'gnome-developer-center'"/></xsl:call-template>
	      </xsl:when>
	      <xsl:otherwise>
		<xsl:call-template name="gettext"><xsl:with-param name="lang"
		  select="@lang"/><xsl:with-param name="msgid"
		  select="'gnome-developer-center'"/></xsl:call-template>
	      </xsl:otherwise>
	    </xsl:choose>
	  </title>
	  <xsl:call-template name="libgo.head">
	    <xsl:with-param name="channel" select="@channel"/>
	  </xsl:call-template>
          <script type="text/javascript" src="/js/strings.js" />
	</head>
	<body class="with-star">
	  <xsl:call-template name="libgo.header">
	    <xsl:with-param name="channel">devel</xsl:with-param>
            <xsl:with-param name="lang" select="@lang"/>
          </xsl:call-template>
	  <div id="container" class="page-wrapper">
	    <div class="container_12">
	    <xsl:if test="title">
	      <div class="page_title"><h1 class="subindex" id="subindex-{@id}"><xsl:value-of select="title"/></h1></div>
	    </xsl:if>
            <div class="content">
	    <xsl:apply-templates select="section" mode="channelindex">
              <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
	      <xsl:sort select="translate(@toc_id, $ucletters, $lcletters)" />
              <xsl:with-param name="lang" select="@lang"/>
	      <xsl:with-param name="hidetitle" select="count(section) = 1"/>
	    </xsl:apply-templates>
	    <xsl:apply-templates select="index" mode="channelindex">
              <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
	    </xsl:apply-templates>
            </div>
	  </div>
          <div>
            <xsl:attribute name="class">
              sidebar
              <xsl:if test="not(title)"> notitle</xsl:if>
            </xsl:attribute>
            <xsl:if test="@channel = 'users'">
              <div id="usr" class="subtle_box">
                <h2><span>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'userslabel'"/></xsl:call-template>
                </span></h2>
                <p>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'userstext'"/></xsl:call-template>
                </p>
              </div>
            </xsl:if>
	    <xsl:if test="$filename = 'references'">
	      <div class="subtle_box">
              <xsl:apply-templates select="." mode="toc">
                <xsl:with-param name="lang" select="@lang"/>
	      </xsl:apply-templates>
	      </div>
	    </xsl:if>
            <xsl:if test="@channel = 'devel'">
              <div id="dev" class="subtle_box">
                <h2><span>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'developerslabel'"/></xsl:call-template>
                </span></h2>
                <p>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'developerstext'"/></xsl:call-template>
                </p>
              </div>
            </xsl:if>

            <xsl:if test="@channel = 'admin'">
              <div id="adm" class="subtle_box">
                <h2><span>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'sysadminslabel'"/></xsl:call-template>
                </span></h2>
                <p>
                  <xsl:call-template name="gettext"><xsl:with-param name="lang"
                    select="@lang"/><xsl:with-param name="msgid"
                    select="'sysadminstext'"/></xsl:call-template>
                </p>
              </div>
            </xsl:if>

          </div>
	  <div id="footer_art" class="default"> </div>
	  </div>
	  <xsl:call-template name="libgo.footer"/>
	</body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="home">
    <xsl:if test="$libgo.debug">
      <xsl:message>Writing home: <xsl:value-of select="concat('index.html.', @lang)" /></xsl:message>
    </xsl:if>
    <exsl:document href="index.html.{@lang}"
        method="html" encoding="UTF-8" indent="yes" omit-xml-declaration="yes"
        doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
        doctype-system="http://www.w3.org/TR/html4/loose.dtd">
      <html lang="{@lang}">
	<head>
	  <title><xsl:call-template name="gettext"><xsl:with-param name="lang"
	  select="@lang"/><xsl:with-param name="msgid"
	  select="'gnome-developer-center'"/></xsl:call-template></title>
	  <xsl:call-template name="libgo.head"/>
	  <xsl:comment>[if IE]&gt;
&lt;style&gt;
div.body-sidebar { width: 100%; }
&lt;/style&gt;
&lt;![endif]</xsl:comment><xsl:text>
</xsl:text>
          <script type="text/javascript" src="/js/strings.js" />
	  <link rel="stylesheet" type="text/css" media="all" href="/skin/960_24_col.css" />
	  <link rel="stylesheet" type="text/css" href="/skin/jquery.autocomplete.css"/>
	  <script type="text/javascript" src="/js/jquery.js"></script>
	  <script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
	  <script type="text/javascript" src="/js/jquery.autocomplete.js"></script>
	</head>
	<body class="with-star">
	  <xsl:call-template name="libgo.header">
	    <xsl:with-param name="channel">devel</xsl:with-param>
            <xsl:with-param name="lang" select="@lang"/>
          </xsl:call-template>
   <div id="wrap">
	  <div id="container">
	    <div class="container_12">
	<dl class="doc-index">
        <xsl:for-each select="//document">
          <xsl:sort select="format-number(@weight, '0.000')" order="descending"/>
          <xsl:sort select="translate(title, $ucletters, $lcletters)"/>
          <xsl:apply-templates select="." mode="channelindex">
            <xsl:with-param name="lang" select="$libgo.lang"/>
            <xsl:with-param name="ignoredeprecated" select="true()"/>
  	</xsl:apply-templates>
        </xsl:for-each>
	</dl>
	  </div>

   </div>
   </div>

   <div id="footer_art" class="default"> </div>
   <xsl:call-template name="libgo.footer"/>
   </body>
      </html>
    </exsl:document>
  </xsl:template>

  <xsl:template match="indexes">
    <xsl:apply-templates select="node()"/>
    <xsl:for-each select="home">
      <xsl:variable name="lang" select="@lang"/>
      <xsl:apply-templates select="../index[@lang = $lang]//document" mode="modindex">
        <xsl:with-param name="lang" select="$lang"/>
      </xsl:apply-templates>
      <xsl:apply-templates select=".." mode="nightly">
        <xsl:with-param name="lang" select="$lang"/>
      </xsl:apply-templates>
      <xsl:apply-templates select=".." mode="deprecated">
        <xsl:with-param name="lang" select="$lang"/>
      </xsl:apply-templates>
    </xsl:for-each>
    <xsl:apply-templates select="." mode="languages"/>
    <xsl:apply-templates select="home" mode="javascript"/>
  </xsl:template>

</xsl:stylesheet>
