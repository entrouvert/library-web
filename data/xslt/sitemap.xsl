<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2011 Frederic Peters <fpeters@0d.be>

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                extension-element-prefixes="exsl"
                xsl:exclude-result-prefixes="sitemap"
                version="1.0">


  <!-- Google sitemap stuff -->
  <xsl:template match="document" mode="sitemap">
    <xsl:if test="@path"> <!-- only local documents -->
      <url xmlns="http://www.google.com/schemas/sitemap/0.84">
        <loc>http://library.gnome.org<xsl:value-of select="@path"
          />index.html.<xsl:value-of select="@lang"/></loc>
        <changefreq>daily</changefreq>
	<priority>0.7</priority>
      </url>
    </xsl:if>
  </xsl:template>

  <xsl:template match="index" mode="sitemap">
    <xsl:param name="channel" select="@channel"/>
    <xsl:param name="lang" select="@lang"/>
    <xsl:param name="filename">
      <xsl:choose>
        <xsl:when test="@id"><xsl:value-of select="@id"/></xsl:when>
        <xsl:otherwise>index</xsl:otherwise>
      </xsl:choose>
    </xsl:param>

    <url xmlns="http://www.google.com/schemas/sitemap/0.84">
      <loc>http://library.gnome.org/<xsl:value-of select="@channel"
        />/<xsl:value-of select="$filename"
        />.html.<xsl:value-of select="@lang"/></loc>
      <priority>0.9</priority>
      <changefreq>daily</changefreq>
    </url>
    <xsl:apply-templates select="index" mode="sitemap"/>
    <xsl:apply-templates select="section/document[@lang = $lang]" mode="sitemap"/>
  </xsl:template>

  <xsl:template match="home" mode="sitemap">
    <xsl:param name="lang" select="@lang"/>
    <url xmlns="http://www.google.com/schemas/sitemap/0.84">
      <loc>http://library.gnome.org/index.html.<xsl:value-of select="$lang"/></loc>
      <priority>1.0</priority>
      <changefreq>daily</changefreq>
    </url>
    <xsl:apply-templates select="../index[@lang = $lang]" mode="sitemap"/>
  </xsl:template>

  <xsl:template match="indexes" mode="sitemap">
    <exsl:document href="sitemap.xml" method="xml" indent="yes" encoding="UTF-8">
      <urlset xmlns="http://www.google.com/schemas/sitemap/0.84"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84
        http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
          <xsl:apply-templates select="home" mode="sitemap"/>
      </urlset>
    </exsl:document>
  </xsl:template>

  <xsl:template match="indexes">
    <xsl:apply-templates select="." mode="sitemap"/>
  </xsl:template>

</xsl:stylesheet>
