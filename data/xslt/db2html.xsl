<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2006 Goran Rakic <grakic@devbase.net>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:set="http://exslt.org/sets"
                xmlns="http://www.w3.org/1999/xhtml"
		xmlns:str="http://exslt.org/strings"
                extension-element-prefixes="exsl"
                xsl:exclude-result-prefixes="set str"
                version="1.0">

<!-- FIXME: Shaun's styles are under LGPL, is it OK to use GPL for this? -->
<xsl:import href="/usr/share/xml/gnome/xslt/docbook/html/db2html.xsl"/>
<xsl:import href="heading.xsl"/>
<xsl:import href="libgo-xref.xsl"/>
<xsl:import href="gettext.xsl"/>

<!-- output -->
<xsl:output method="html" encoding="UTF-8" indent="yes"
        omit-xml-declaration="yes"
	doctype-public="-//W3C//DTD HTML 4.01 Transitional//EN"
	doctype-system="http://www.w3.org/TR/html4/loose.dtd"/>

<!-- This gets set on the command line ... -->
<xsl:param name="libgo.lang" select="''"/>
<xsl:param name="libgo.tarball" select="false()"/>
<xsl:param name="libgo.languages_in_sidebar" select="false()"/>
<xsl:param name="libgo.friends_of_gnome_ruler" select="false()"/>
<xsl:param name="libgo.hide_section_numbers" select="false()"/>
<xsl:param name="libgo.nightly" select="false()"/>

<!-- Setting parameters for included stylesheets -->
<xsl:param name="db.chunk.extension" select="concat('.html.',$libgo.lang)"/>
<xsl:param name="theme.icon.admon.path" select="'/skin/'"/>
<xsl:param name="theme.icon.nav.previous" select="'/skin/nav-previous.png'"/>
<xsl:param name="theme.icon.nav.next" select="'/skin/nav-next.png'"/>

<!-- Get abstract/title -->
<xsl:template name="libgo.info.abstract">
  <xsl:param name="node" select="."/>
  <xsl:param name="info" select="'FIXME'"/>
  <xsl:apply-templates mode="db2html.info.mode"
                       select="$info/abstract/node()"/>
</xsl:template>

<xsl:template name="libgo.info.title">
  <xsl:param name="node" select="."/>
  <xsl:param name="info" select="'FIXME'"/>
  <xsl:choose>
    <xsl:when test="$info/title/node()">
      <xsl:apply-templates mode="db2html.info.mode"
        select="$info/title[not(preceding-sibling::title)]/node()"/>
    </xsl:when>
    <xsl:otherwise>
      <!-- gnome-user-docs (user-guide, system-admin-guide and accessibility-guide
           don't have title in bookinfo but directly as a child of <book> -->
      <xsl:apply-templates mode="db2html.info.mode" select="$info/../title/node()"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- Create XML index file -->
<xsl:template name="libgo.indexfile">
  <xsl:param name="info"/>
  <xsl:param name="title">
    <xsl:call-template name="libgo.info.title">
      <xsl:with-param name="info" select="$info"/>
    </xsl:call-template>
  </xsl:param>  
  <xsl:param name="abstract">
    <xsl:call-template name="libgo.info.abstract">
      <xsl:with-param name="info" select="$info"/>
    </xsl:call-template>
  </xsl:param>  
  <exsl:document href="{concat('index.xml.',$libgo.lang)}" indent="yes" >
    <document index="{$info/../@id}">
      <title>
        <xsl:value-of select="normalize-space($title)" />
      </title>
      <abstract>
        <xsl:value-of select="normalize-space($abstract)" />
      </abstract>
    </document>
  </exsl:document>    
</xsl:template>

<xsl:template match="i">
 <i><xsl:apply-templates/></i>
</xsl:template>

<!-- special case the list of languages as published in the release notes,
     so language names are sorted -->
<xsl:template match="itemizedlist[@id='langlist']">
  <div class="block list itemizedlist">
    <ul class="itemizedlist">
       <xsl:for-each select="listitem">
         <xsl:sort select="para" lang="$libgo.lang"/>
	 <li><xsl:value-of select="para"/></li>
       </xsl:for-each>
    </ul>
  </div>
</xsl:template>

<xsl:template name="db2html.ulink" match="ulink">
  <xsl:param name="url">
    <xsl:choose>
      <xsl:when test="substring(@url, 0, 7) = 'ghelp:'">
        <xsl:apply-templates name="libgo.xref" select="@url"/>
      </xsl:when>
      <xsl:otherwise><xsl:value-of select="@url"/></xsl:otherwise>
    </xsl:choose>
  </xsl:param>
  <xsl:param name="content" select="false()"/>
  <a class="ulink" href="{$url}">
    <xsl:attribute name="title">
      <xsl:call-template name="db.ulink.tooltip"/>
    </xsl:attribute>
    <xsl:choose>
      <xsl:when test="$content">
        <xsl:copy-of select="$content"/>
      </xsl:when>
      <xsl:when test="string-length(normalize-space(node())) != 0">
        <xsl:apply-templates/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$url"/>
      </xsl:otherwise>
    </xsl:choose>
  </a>
</xsl:template>

<xsl:template match="@url[substring(., 0, 7) = 'ghelp']">
  <xsl:message>TTTT</xsl:message>
</xsl:template>

<!-- Process -->
<xsl:template match="/">
  <xsl:call-template name="db.chunk">
    <xsl:with-param name="node" select="*[1]"/>
    <xsl:with-param name="depth_of_chunk" select="0"/>
  </xsl:call-template>
  <xsl:if test="not($libgo.tarball)">
    <xsl:call-template name="libgo.indexfile">
      <xsl:with-param name="info" select="article/articleinfo|book/bookinfo"/>
    </xsl:call-template>
  </xsl:if>
</xsl:template>

<!-- FIXME: when we override db2html.css, do this there -->
<xsl:template name="db2html.division.head.extra">
  <xsl:if test="not($libgo.tarball)">
    <xsl:call-template name="libgo.head"/>
    <xsl:if test="$libgo.friends_of_gnome_ruler">
<style type="text/css">
div.sidebar { top: 230px; }
</style>
    </xsl:if>
    <xsl:if test="$libgo.hide_section_numbers">
<style type="text/css">
span.label { display: none; }
</style>
    </xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template name="db2html.division.top">
  <xsl:if test="not($libgo.tarball)">
    <xsl:call-template name="libgo.header">
      <xsl:with-param name="lang"><xsl:value-of select="$libgo.lang"/></xsl:with-param>
    </xsl:call-template>
    <xsl:if test="$libgo.friends_of_gnome_ruler">
      <div id="libgo-ruler">
      <script type="text/javascript" src="http://www.gnome.org/friends/ruler/ruler.js">vuntz is my hero</script>
      </div>
    </xsl:if>
  </xsl:if>
</xsl:template>

<xsl:template name="db2html.sidenav">
  <xsl:param name="node" select="."/>
  <xsl:param name="template"/>
  <div class="sidenav">
    <xsl:if test="$libgo.nightly">
      <p class="caution-nightly">
        Nightly Build
	<br/>
	<xsl:choose>
	  <xsl:when test="//releaseinfo/@role">
	    Status: <xsl:value-of select="//releaseinfo/@role"/>
	  </xsl:when>
	  <xsl:otherwise>
	    Work In Progress
	  </xsl:otherwise>
	</xsl:choose>
      </p>
    </xsl:if>

    <xsl:call-template name="db2html.autotoc">
      <xsl:with-param name="node" select="/"/>
      <xsl:with-param name="show_info" select="$db.chunk.info_chunk"/>
      <xsl:with-param name="is_info" select="$template = 'info'"/>
      <xsl:with-param name="selected" select="$node"/>
      <xsl:with-param name="divisions" select="/*"/>
      <xsl:with-param name="toc_depth" select="$db.chunk.max_depth + 1"/>
      <xsl:with-param name="labels" select="false()"/>
      <xsl:with-param name="titleabbrev" select="true()"/>
    </xsl:call-template>
    <xsl:if test="$libgo.languages_in_sidebar">
      <h4>
        <xsl:call-template name="gettext"><xsl:with-param name="lang"
        select="$libgo.lang"/><xsl:with-param name="msgid"
        select="'langinfo'"/></xsl:call-template>
      </h4>
      <ul class="i18n">
      <xsl:for-each select="str:split($libgo.languages_in_sidebar, ',')">
        <li><a href="index.html.{node()}">
          <xsl:call-template name="language-label">
            <xsl:with-param name="lang" select="."/>
          </xsl:call-template>
          </a>
          <xsl:text> </xsl:text>
          <span class="lang-code">(<xsl:value-of select="."/>)</span>
        </li>
      </xsl:for-each>
      </ul>
    </xsl:if>
  </div>
</xsl:template>

  <xsl:template name="language-label">
    <xsl:param name="lang"/>
    <xsl:choose>
      <xsl:when test="document('../languages.xml')//lang[@code = $lang]">
        <xsl:value-of select="document('../languages.xml')//lang[@code =
        $lang]"/>
      </xsl:when>
      <xsl:when test="document('../languages.xml')//lang[substring(@code, 1, 2) = $lang]">
        <xsl:value-of
        select="document('../languages.xml')//lang[substring(@code, 1, 2) =
        $lang]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$lang"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

<!-- modified to add a language extension to images -->
<xsl:template name="db2html.imagedata.src">
  <xsl:param name="node" select="."/>
  <xsl:choose>
    <xsl:when test="$node/@fileref">
      <!-- FIXME: do this less stupidly, or not at all -->
      <xsl:choose>
        <xsl:when test="$node/@format = 'PNG' and
                        (substring($node/@fileref, string-length($node/@fileref) - 3)
                          != '.png')">
          <xsl:value-of select="concat($node/@fileref, '.png.', $libgo.lang)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat($node/@fileref, '.', $libgo.lang)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:when test="$node/@entityref">
      <xsl:value-of select="unparsed-entity-uri($node/@entityref)"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>

<!-- modified from common/db-xref.xsl to avoid filename in link when generating
     a flat file-->
<xsl:template name="db.xref.target">
  <xsl:param name="linkend" select="@linkend"/>
  <xsl:param name="target" select="key('idkey', $linkend)"/>
  <xsl:param name="is_chunk" select="false()"/>
  <xsl:choose>
    <xsl:when test="$linkend = $db.chunk.info_basename">
      <xsl:value-of
       select="concat($db.chunk.info_basename, $db.chunk.extension)"/>
    </xsl:when>
    <xsl:when test="set:has-same-node($target, /*)">
      <xsl:value-of select="concat($db.chunk.basename, $db.chunk.extension)"/>
    </xsl:when>
    <xsl:when test="$is_chunk">
      <xsl:value-of select="concat($linkend, $db.chunk.extension)"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="target_chunk_id">
        <xsl:call-template name="db.chunk.chunk-id">
          <xsl:with-param name="node" select="$target"/>
        </xsl:call-template>
      </xsl:variable>
      <!-- NOTE: this is the difference with standard db.xref.target -->
      <xsl:if test="$db.chunk.max_depth != 0 or $target_chunk_id != 'index'">
        <xsl:value-of select="concat($target_chunk_id, $db.chunk.extension)"/>
      </xsl:if>
      <xsl:if test="string($linkend) != '' and string($target_chunk_id) != string($linkend)">
        <xsl:value-of select="concat('#', $linkend)"/>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- hack around the libxslt(?) version installed on window.gnome.org (RH 5.3) (see bug 537091) -->
<xsl:template name="db2html.anchor" match="anchor">
  <xsl:param name="node" select="."/>
  <xsl:param name="name" select="$node/@id"/>
  <xsl:if test="$name"><a name="{$name}"><xsl:comment><xsl:value-of select="$name"/></xsl:comment></a></xsl:if>
</xsl:template>

<xsl:template mode="db.label.mode" match="book">
 <!-- don't output anything for this element as gnome-doc-utils calls gettext
      on a missing msgid which is then rendered as is.
      See bug http://bugzilla.gnome.org/show_bug.cgi?id=540488
   -->
</xsl:template>

<xsl:template match="remark">
  <xsl:if test="$libgo.nightly">
    <div class="devel-remark">
      <xsl:apply-templates/>
    </div>
  </xsl:if>
</xsl:template>

<xsl:template name="db2html.division.html">
  <xsl:param name="node" select="."/>
  <xsl:param name="info" select="/false"/>
  <xsl:param name="template"/>
  <xsl:param name="depth_of_chunk">
    <xsl:call-template name="db.chunk.depth-of-chunk">
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
  </xsl:param>
  <xsl:param name="prev_id">
    <xsl:choose>
      <xsl:when test="$depth_of_chunk = 0">
        <xsl:if test="$info and $db.chunk.info_chunk">
          <xsl:value-of select="$db.chunk.info_basename"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="db.chunk.chunk-id.axis">
          <xsl:with-param name="node" select="$node"/>
          <xsl:with-param name="axis" select="'previous'"/>
          <xsl:with-param name="depth_in_chunk" select="0"/>
          <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:param>
  <xsl:param name="next_id">
    <xsl:call-template name="db.chunk.chunk-id.axis">
      <xsl:with-param name="node" select="$node"/>
      <xsl:with-param name="axis" select="'next'"/>
      <xsl:with-param name="depth_in_chunk" select="0"/>
      <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
    </xsl:call-template>
  </xsl:param>
  <xsl:variable name="prev_node" select="key('idkey', $prev_id)"/>
  <xsl:variable name="next_node" select="key('idkey', $next_id)"/>
  <!-- FIXME -->
  <html>
    <head>
      <title>
        <xsl:variable name="title">
          <xsl:call-template name="db.title">
            <xsl:with-param name="node" select="$node"/>
          </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="normalize-space($title)"/>
      </title>
      <xsl:if test="string($prev_id) != ''">
        <link rel="previous">
          <xsl:attribute name="href">
            <xsl:call-template name="db.xref.target">
              <xsl:with-param name="linkend" select="$prev_id"/>
              <xsl:with-param name="target" select="$prev_node"/>
              <xsl:with-param name="is_chunk" select="true()"/>
            </xsl:call-template>
          </xsl:attribute>
          <xsl:attribute name="title">
            <xsl:call-template name="db.title">
              <xsl:with-param name="node" select="$prev_node"/>
            </xsl:call-template>
          </xsl:attribute>
        </link>
      </xsl:if>
      <xsl:if test="string($next_id) != ''">
        <link rel="next">
          <xsl:attribute name="href">
            <xsl:call-template name="db.xref.target">
              <xsl:with-param name="linkend" select="$next_id"/>
              <xsl:with-param name="target" select="$next_node"/>
              <xsl:with-param name="is_chunk" select="true()"/>
            </xsl:call-template>
          </xsl:attribute>
          <xsl:attribute name="title">
            <xsl:call-template name="db.title">
              <xsl:with-param name="node" select="$next_node"/>
            </xsl:call-template>
          </xsl:attribute>
        </link>
      </xsl:if>
      <xsl:if test="/*[1] != $node">
        <link rel="top">
          <xsl:attribute name="href">
            <xsl:call-template name="db.xref.target">
              <xsl:with-param name="linkend" select="/*[1]/@id"/>
              <xsl:with-param name="target" select="/*[1]"/>
              <xsl:with-param name="is_chunk" select="true()"/>
            </xsl:call-template>
          </xsl:attribute>
          <xsl:attribute name="title">
            <xsl:call-template name="db.title">
              <xsl:with-param name="node" select="/*[1]"/>
            </xsl:call-template>
          </xsl:attribute>
        </link>
      </xsl:if>
      <xsl:call-template name="db2html.css">
        <xsl:with-param name="css_file" select="$depth_of_chunk = 0"/>
      </xsl:call-template>
      <xsl:call-template name="db2html.division.head.extra"/>
    </head>
    <body>
      <xsl:call-template name="db2html.division.top">
        <xsl:with-param name="node" select="$node"/>
        <xsl:with-param name="info" select="$info"/>
        <xsl:with-param name="template" select="$template"/>
        <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
        <xsl:with-param name="prev_id" select="$prev_id"/>
        <xsl:with-param name="next_id" select="$next_id"/>
        <xsl:with-param name="prev_node" select="$prev_node"/>
        <xsl:with-param name="next_node" select="$next_node"/>
      </xsl:call-template>
      <xsl:variable name="sidebar">
        <xsl:call-template name="db2html.division.sidebar">
          <xsl:with-param name="node" select="$node"/>
          <xsl:with-param name="info" select="$info"/>
          <xsl:with-param name="template" select="$template"/>
          <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
          <xsl:with-param name="prev_id" select="$prev_id"/>
          <xsl:with-param name="next_id" select="$next_id"/>
          <xsl:with-param name="prev_node" select="$prev_node"/>
          <xsl:with-param name="next_node" select="$next_node"/>
        </xsl:call-template>
      </xsl:variable>
      <div id="container" class="two_columns">
        <xsl:attribute name="class">
          <xsl:if test="$sidebar != ''">
            <xsl:text>two_columns</xsl:text>
          </xsl:if>
        </xsl:attribute>
        <div class="content">
        <xsl:choose>
          <xsl:when test="$template = 'info'">
            <xsl:call-template name="db2html.info.div">
              <xsl:with-param name="node" select="$node"/>
              <xsl:with-param name="info" select="$info"/>
              <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="$node">
              <xsl:with-param name="depth_in_chunk" select="0"/>
              <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
            </xsl:apply-templates>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:call-template name="db2html.division.bottom">
          <xsl:with-param name="node" select="$node"/>
          <xsl:with-param name="info" select="$info"/>
          <xsl:with-param name="template" select="$template"/>
          <xsl:with-param name="depth_of_chunk" select="$depth_of_chunk"/>
          <xsl:with-param name="prev_id" select="$prev_id"/>
          <xsl:with-param name="next_id" select="$next_id"/>
          <xsl:with-param name="prev_node" select="$prev_node"/>
          <xsl:with-param name="next_node" select="$next_node"/>
        </xsl:call-template>
        </div>
        <xsl:copy-of select="$sidebar"/>
        <div id="footer_art" class="default"> </div>
      </div>
      <xsl:call-template name="libgo.footer"/>
    </body>
  </html>
</xsl:template>

</xsl:stylesheet>
