<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2011 Frederic Peters <fpeters@0d.be>

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                extension-element-prefixes="exsl"
                version="1.0">

  <!-- robots.txt stuff -->

  <xsl:template match="document" mode="robotstxt">
    <xsl:if test="versions">
      <xsl:variable name="basedir">/<xsl:value-of  select="@channel"
      />/<xsl:value-of select="@modulename" />/</xsl:variable>
      <xsl:for-each select="versions/version">
        <xsl:text>Disallow: </xsl:text>
	<xsl:value-of select="$basedir"/><xsl:value-of select="@href"/>
	<xsl:text>/
</xsl:text>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template match="indexes" mode="robotstxt">
    <xsl:param name="onelang"><xsl:value-of select="index[position() = 1]/@lang"/></xsl:param>
    <xsl:message>Writing robots.txt</xsl:message>
    <exsl:document href="robots.txt" method="text" encoding="UTF-8">
      <xsl:text># don't let robot index all versions of documents
# see http://bugzilla.gnome.org/show_bug.cgi?id=509424
User-agent: *
</xsl:text>
      <xsl:apply-templates select="index[@lang = $onelang and @channel != 'misc']//document" mode="robotstxt"/>
    </exsl:document>
  </xsl:template>

  <xsl:template match="indexes">
    <xsl:apply-templates select="." mode="robotstxt"/>
  </xsl:template>

</xsl:stylesheet>
