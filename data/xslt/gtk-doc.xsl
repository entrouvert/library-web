<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
Copyright (c) 2006 Goran Rakic <grakic@devbase.net>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://www.w3.org/1999/xhtml"
                version="1.0">

  <xsl:import href="/usr/share/gtk-doc/data/gtk-doc.xsl"/>
  <xsl:import href="heading.xsl"/>

  <xsl:param name="libgo.lang" select="'en'"/>
  <xsl:param name="default.encoding" select="'UTF-8'"/>
  <xsl:param name="chunker.output.encoding" select="'UTF-8'"/>
  <xsl:param name="html.ext" select="'.html'"/>
  
  <!-- Create XML index file -->
  <xsl:template match="bookinfo|articleinfo">
    <xsl:call-template name="write.chunk">
      <xsl:with-param name="filename" select="'index.xml.en'"/>
      <xsl:with-param name="method" select="'xml'"/>
      <xsl:with-param name="indent" select="'yes'"/>
      <xsl:with-param name="content">
        <document>
          <title>
            <xsl:value-of select="normalize-space(title)" />
          </title>
          <abstract>
            <xsl:value-of select="normalize-space(releaseinfo)" />
          </abstract>
        </document>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="user.header.navigation">
    <xsl:call-template name="libgo.header" />
  </xsl:template>

  <xsl:template name="user.head.content">
    <xsl:if test="$gtkdoc.version">
      <meta name="generator"
            content="GTK-Doc V{$gtkdoc.version} (XML mode)"/>
    </xsl:if>

    <xsl:call-template name="libgo.head"/>


      <!-- copied from the html.head template in the docbook stylesheets
           we don't want links for all refentrys, thats just too much
        -->
      <xsl:variable name="this" select="."/>
      <xsl:for-each select="//part
                            |//reference
                            |//preface
                            |//chapter
                            |//article
                            |//appendix[not(parent::article)]|appendix
                            |//glossary[not(parent::article)]|glossary
                            |//index[not(parent::article)]|index">
        <link rel="{local-name(.)}">
          <xsl:attribute name="href">
            <xsl:call-template name="href.target">
              <xsl:with-param name="context" select="$this"/>
              <xsl:with-param name="object" select="."/>
            </xsl:call-template>
          </xsl:attribute>
          <xsl:attribute name="title">
            <xsl:apply-templates select="." mode="object.title.markup.textonly"/>
          </xsl:attribute>
        </link>
      </xsl:for-each>
  </xsl:template>

  <xsl:template name="user.footer.content">
  </xsl:template>

<!-- overriding the one from docbook-xsl/html/chunk-common.xsl -->
<xsl:template name="chunk-element-content">
  <xsl:param name="prev"/>
  <xsl:param name="next"/>
  <xsl:param name="nav.context"/>
  <xsl:param name="content">
    <xsl:apply-imports/>
  </xsl:param>

  <xsl:call-template name="user.preroot"/>

  <html>
    <xsl:call-template name="html.head">
      <xsl:with-param name="prev" select="$prev"/>
      <xsl:with-param name="next" select="$next"/>
    </xsl:call-template>

    <body>
      <xsl:call-template name="libgo.header" />

      <div id="container">

      <xsl:call-template name="header.navigation">
        <xsl:with-param name="prev" select="$prev"/>
        <xsl:with-param name="next" select="$next"/>
        <xsl:with-param name="nav.context" select="$nav.context"/>
      </xsl:call-template>

      <xsl:call-template name="user.header.content"/>

      <xsl:copy-of select="$content"/>

      <xsl:call-template name="user.footer.content"/>

      <xsl:call-template name="footer.navigation">
        <xsl:with-param name="prev" select="$prev"/>
        <xsl:with-param name="next" select="$next"/>
        <xsl:with-param name="nav.context" select="$nav.context"/>
      </xsl:call-template>

      <xsl:call-template name="user.footer.navigation"/>
      <div id="footer_art" class="default"> </div>
      </div>
      <xsl:call-template name="libgo.footer"/>
    </body>
  </html>
  <xsl:value-of select="$chunk.append"/>
</xsl:template>



</xsl:stylesheet>
