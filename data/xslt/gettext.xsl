<?xml version='1.0' encoding='UTF-8'?>
<!--
Copyright (c) 2007 Frederic Peters <fpeters@0d.be>.

This file is part of libgo.

libgo is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

libgo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with libgo; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:html="http://www.w3.org/1999/xhtml"
                extension-element-prefixes="exsl"
                version="1.0">

  <xsl:template name="gettext">
    <xsl:param name="lang"/>
    <xsl:param name="msgid"/>
    <xsl:choose>
      <xsl:when test="document('../catalog.xml')/msgcat/msgstr[@msgid = $msgid and @xml:lang = $lang]">
        <xsl:value-of select="document('../catalog.xml')/msgcat/msgstr[@msgid = $msgid and @xml:lang = $lang]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="document('../catalog.xml')/msgcat/msgstr[@msgid = $msgid]"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>
