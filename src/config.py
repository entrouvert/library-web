# libgo - script to build library.gnome.org
# Copyright (C) 2007  Frederic Peters
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301  USA
#
# based on jhbuild/config.py from JHBuild, licensed under the same terms,
#   Copyright (C) 2001-2006  James Henstridge

import os
import traceback

from errors import FatalError

_defaults_file = os.path.join(os.path.dirname(__file__), 'defaults.lgorc')
if 'HOME' in os.environ:
    _default_lgorc = os.path.join(os.environ['HOME'], '.lgorc')
else:
    _default_lgorc = None

_known_keys = ['ftp_gnome_org_local_copy', 'use_latest_version',
            'private_dir', 'download_cache_dir', 'output_dir',
            'version_min', 'version_max', 'modules', 'languages',
            'blacklist', 'extra_tarballs', 'symbols_dbm_filepath',
            'httxt2dbm_path', 'fast_mode', 'create_tarballs',
            'symbols_sqlite_filepath', 'nightly_tarballs_location',
            'channels', 'doc_path_template', 'indexes_xsl_file']

class Config:
    def __init__(self, filename=_default_lgorc):
        config = {
            '__file__': _default_lgorc,
            '__datadir__': os.path.join(os.path.dirname(__file__), '../data/'),
        }
        try:
            execfile(_defaults_file, config)
        except:
            traceback.print_exc()
            raise FatalError('could not load config defaults')

        if os.path.exists(filename):
            config['__file__'] = filename
            try:
                execfile(filename, config)
            except:
                traceback.print_exc()
                raise FatalError('could not load config file')

        for name in _known_keys:
            setattr(self, name, config[name])

